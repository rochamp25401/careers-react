import React from "react";
import { shallow } from "enzyme";
import TypingPage from "./TypingPage";

describe("TypingPage", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<TypingPage />);
    expect(wrapper).toMatchSnapshot();
  });
});
