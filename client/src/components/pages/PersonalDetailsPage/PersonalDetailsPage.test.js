import React from "react";
import { shallow } from "enzyme";
import PersonalDetailsPage from "./PersonalDetailsPage";

describe("PersonalDetailsPage", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<PersonalDetailsPage />);
    expect(wrapper).toMatchSnapshot();
  });
});
