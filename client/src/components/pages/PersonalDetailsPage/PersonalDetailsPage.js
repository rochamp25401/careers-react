import React, { useEffect, useState } from "react";
import { InputGroup, InputGroupText } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import { useForm, Controller } from "react-hook-form";
import ReactDatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Moment from "moment";
import * as yup from "yup";
import { useWindowScroll } from "react-use";
import ScrollToTop from "react-scroll-to-top";
import { yupResolver } from "@hookform/resolvers/yup";
import * as personalActions from "./../../../actions/personal.action";
import { makeStyles } from "@material-ui/core/styles";
import ListSubheader from "@material-ui/core/ListSubheader";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import DraftsIcon from "@material-ui/icons/Drafts";
import SendIcon from "@material-ui/icons/Send";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import StarBorder from "@material-ui/icons/StarBorder";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#fff",
    fontSize: "12px",
    fontFamily:
      "Kanit, OpenSans, Helvetica Neue, Helvetica Arial, sans-serif !important",
  },
  nested: {
    paddingLeft: theme.spacing(5),
  },
}));

const styles = {
  error: {
    color: "#bf1650",
  },
};



export default function PersonalDetailsPage() {
  const { personalReducer } = useSelector((personalReducer) => personalReducer);
  const dispatch = useDispatch();
  const classes = useStyles();
  const { x, y } = useWindowScroll();
  const [nav1, setNav1] = useState(true);
  const [nav2, setNav2] = useState(true);
  const [nav3, setNav3] = useState(true);
  const [otherInitial, setOtherInitial] = useState(true);
  const [diffday, setDiffday] = useState(0);
  const [isMilitary, setisMilitary] = useState(true);
  const [isMarried, setisMarried] = useState(true);
  const [Relatives, setRelatives] = useState(0);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [startDate2, setStartDate2] = useState(null);
  const [endDate2, setEndDate2] = useState(null);
  const [values, setInitialOtherValues] = useState(null);
  const [militaryOther, setMilitaryOtherValues] = useState(null);
  const [maritalOtherFname, setMaritalOtherFname] = useState(null);
  const [maritalOtherLname, setMaritalOtherLname] = useState(null);
  const [maritalOtherAge, setMaritalOtherAge] = useState(null);
  const [maritalOtherOccupation, setMaritalOtherOccupation] = useState(null);
  const [maritalOtherCompany, setMaritalOtherCompany] = useState(null);
  const [maritalOtherPhoneNo, setMaritalOtherPhoneNo] = useState(null);
  const [children, setChildren] = useState(null);
  const [boy, setBoy] = useState(null);
  const [girl, setGirl] = useState(null);
  const [phoneNoExt, setPhoneNoExt] = useState("");
  const [validateInitial, setValidateInitial] = useState(false);
  const [validateOther, setValidateOther] = useState(false)
  const [validateMarried, setValidateMarried] = useState(false);
  var rowRelatives = [];
  

  const schema = yup.object().shape({
    candidateId: yup.string(),
    initialId: yup.number().typeError("required field"),
    initialOtherDetail: yup.string().when("initialId", {
      is: (initialId) => initialId == 4,
      then: yup.string().required("required field"),
      otherwise: yup.string().nullable(),
    }),
    nameTh: yup.string().required("required field"),
    surnameTh: yup.string().required("required field"),
    nicknameTh: yup.string().required("required field"),
    nameEng: yup.string().required("required field"),
    surnameEng: yup.string().required("required field"),
    birthday: yup.string().typeError("required field"),
    weight: yup
      .string()
      .required("required field")
      .matches(/^[0-9]+$/, "required number"),
    height: yup
      .string()
      .required("required field")
      .matches(/^[0-9]+$/, "required number"),
    bloodGroup: yup.string().required("required field"),
    nationality: yup.string().required("required field"),
    religion: yup.string().required("required field"),
    hometown: yup.number().typeError("required field"),
    idCard: yup
      .string()
      .required("required field")
      .matches(/^[0-9]+$/, "required number")
      .test(
        "len",
        "Must be exactly 13 characters",
        (val) => val.toString().length === 13
      ),
    dateOfIssue: yup.string().nullable(),
    expired: yup.string().nullable(),
    issuedBy: yup.string().required("required field"),
    provincePersonal: yup.number().typeError("required field"),
    passportNo: yup.string(),
    passportStart: yup.string().nullable(),
    passportExp: yup.string().nullable(),
    militaryStatusID: yup.number().typeError("required field"),
    militaryOtherStatus: yup.string().when("militaryStatusID", {
      is: (militaryStatusID) => militaryStatusID == 6,
      then: yup.string().required("required field"),
    }),
    maritalId: yup.number().typeError("required field"),
    name: yup.string().when("maritalId", {
      is: (maritalId) => maritalId == 2,
      then: yup.string().required("required field"),
    }),
    surname: yup.string().when("maritalId", {
      is: (maritalId) => maritalId == 2,
      then: yup.string().required("required field"),
    }),
    age: yup.string().when("maritalId", {
      is: (maritalId) => maritalId == 2,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number"),
    }),
    occupation: yup.string().when("maritalId", {
      is: (maritalId) => maritalId == 2,
      then: yup.string().required("required field"),
    }),
    company: yup.string().when("maritalId", {
      is: (maritalId) => maritalId == 2,
      then: yup.string().required("required field"),
    }),
    phoneNo: yup.string().when("maritalId", {
      is: (maritalId) => maritalId == 2,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number")
        .test(
          "len",
          "Must be exactly 10 characters",
          (val) => val.toString().length === 10
        ),
    }),
    noOfChildren: yup.string().when("maritalId", {
      is: (maritalId) => maritalId == 2,
      then: yup.string().required("required field"),
    }),
    noOfBoys: yup.string().when("maritalId", {
      is: (maritalId) => maritalId == 2,
      then: yup.string().required("required field"),
    }),
    noOfGirls: yup.string().when("maritalId", {
      is: (maritalId) => maritalId == 2,
      then: yup.string().required("required field"),
    }),
  });
  const schema2 = yup.object().shape({
    candidateId: yup.string(),
    houseNo: yup.string().required("required field"),
    villageNo: yup.string(),
    building: yup.string(),
    roomNo: yup.string(),
    floor: yup.string(),
    village: yup.string(),
    lane: yup.string(),
    road: yup.string().required("required field"),
    subArea: yup.string().required("required field"),
    area: yup.string().required("required field"),
    province: yup.number().typeError("required field"),
    zipCode: yup
      .string()
      .required("required field")
      .matches(/^[0-9]+$/, "required number")
      .test(
        "len",
        "Must be exactly 5 characters",
        (val) => val.toString().length === 5
      ),
    phoneNo: yup
      .string()
      .test(
        "num",
        "required number",
        (val) => {
          if (val == null || val == "") return true
          else return Boolean(val.match(/^[0-9]+$/))
        }
      ) 
      .test(
        "len",
        "Must be exactly 10 characters",
        (val) => {
          if (val == null || val == "") return true
          else {
            setPhoneNoExt(val);
            return val.toString().length === 10
          }
        }
      )
  ,
    ext: yup.string(),
  });
  const schema3 = yup.object().shape({
    candidateId: yup.string(),
    fatherName: yup.string().required("required field"),
    fatherSurname: yup.string().required("required field"),
    ageF: yup
      .string()
      .matches(/^[0-9]+$/, "required number")
      .typeError("required field"),
    statusF: yup.string().required("required field"),
    occupationF: yup.string().required("required field"),
    phoneNoF: yup
      .string()
      .matches(/^[0-9]+$/, "required number")
      .test(
        "len",
        "Must be exactly 10 characters",
        (val) => val.toString().length === 10
      )
      .required("required field"),
    motherName: yup.string().required("required field"),
    motherSurname: yup.string().required("required field"),
    ageM: yup
      .string()
      .matches(/^[0-9]+$/, "required number")
      .typeError("required field"),
    statusM: yup.string().required("required field"),
    occupationM: yup.string().required("required field"),
    phoneNoM: yup
      .string()
      .matches(/^[0-9]+$/, "required number")
      .test(
        "len",
        "Must be exactly 10 characters",
        (val) => val.toString().length === 10
      )
      .required("required field"),
    noOfRelatives: yup
      .string()
      .matches(/^[0-9]+$/, "required number")
      .typeError("required field"),
    //--------------------------------------------------------------------
    nameSurname1: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 1,
      then: yup.string().required("required field"),
    }),
    age1: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 1,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number"),
    }),
    occupation1: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 1,
      then: yup.string().required("required field"),
    }),
    phoneNo1: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 1,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number")
        .test(
          "len",
          "Must be exactly 10 characters",
          (val) => val.toString().length === 10
        ),
    }),
    nameSurname2: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 2,
      then: yup.string().required("required field"),
    }),
    age2: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 2,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number"),
    }),
    occupation2: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 2,
      then: yup.string().required("required field"),
    }),
    phoneNo2: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 2,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number")
        .test(
          "len",
          "Must be exactly 10 characters",
          (val) => val.toString().length === 10
        ),
    }),
    nameSurname3: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 3,
      then: yup.string().required("required field"),
    }),
    age3: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 3,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number"),
    }),
    occupation3: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 3,
      then: yup.string().required("required field"),
    }),
    phoneNo3: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 3,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number")
        .test(
          "len",
          "Must be exactly 10 characters",
          (val) => val.toString().length === 10
        ),
    }),
    nameSurname4: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 4,
      then: yup.string().required("required field"),
    }),
    age4: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 4,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number"),
    }),
    occupation4: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 4,
      then: yup.string().required("required field"),
    }),
    phoneNo4: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 4,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number")
        .test(
          "len",
          "Must be exactly 10 characters",
          (val) => val.toString().length === 10
        ),
    }),
    nameSurname5: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 5,
      then: yup.string().required("required field"),
    }),
    age5: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 5,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number"),
    }),
    occupation5: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 5,
      then: yup.string().required("required field"),
    }),
    phoneNo5: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 5,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number")
        .test(
          "len",
          "Must be exactly 10 characters",
          (val) => val.toString().length === 10
        ),
    }),
    nameSurname6: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 6,
      then: yup.string().required("required field"),
    }),
    age6: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 6,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number"),
    }),
    occupation6: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 6,
      then: yup.string().required("required field"),
    }),
    phoneNo6: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 6,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number")
        .test(
          "len",
          "Must be exactly 10 characters",
          (val) => val.toString().length === 10
        ),
    }),
    nameSurname7: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 7,
      then: yup.string().required("required field"),
    }),
    age7: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 7,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number"),
    }),
    occupation7: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 7,
      then: yup.string().required("required field"),
    }),
    phoneNo7: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 7,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number")
        .test(
          "len",
          "Must be exactly 10 characters",
          (val) => val.toString().length === 10
        ),
    }),
    nameSurname8: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 8,
      then: yup.string().required("required field"),
    }),
    age8: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 8,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number"),
    }),
    occupation8: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 8,
      then: yup.string().required("required field"),
    }),
    phoneNo8: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 8,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number")
        .test(
          "len",
          "Must be exactly 10 characters",
          (val) => val.toString().length === 10
        ),
    }),
    nameSurname9: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 9,
      then: yup.string().required("required field"),
    }),
    age9: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 9,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number"),
    }),
    occupation9: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 9,
      then: yup.string().required("required field"),
    }),
    phoneNo9: yup.string().when("noOfRelatives", {
      is: (noOfRelatives) => noOfRelatives >= 9,
      then: yup
        .string()
        .required("required field")
        .matches(/^[0-9]+$/, "required number")
        .test(
          "len",
          "Must be exactly 10 characters",
          (val) => val.toString().length === 10
        ),
    }),
  });

  const { register, handleSubmit, errors, control, setValue, reset } = useForm({
    resolver: yupResolver(schema),
    mode: "onTouched",
  });
  const {
    register: register2,
    errors: errors2,
    handleSubmit: handleSubmit2,
    reset: reset2,
  } = useForm({
    resolver: yupResolver(schema2),
    mode: "onTouched",
  });
  const {
    register: register3,
    errors: errors3,
    handleSubmit: handleSubmit3,
    reset: reset3,
  } = useForm({
    resolver: yupResolver(schema3),
    defaultValues: {
      nameSurname1: "",
      age1: "",
      occupation1: "",
      phoneNo1: "",
      nameSurname2: "",
      age2: "",
      occupation2: "",
      phoneNo2: "",
      nameSurname3: "",
      age3: "",
      occupation3: "",
      phoneNo3: "",
      nameSurname4: "",
      age4: "",
      occupation4: "",
      phoneNo4: "",
      nameSurname5: "",
      age5: "",
      occupation5: "",
      phoneNo5: "",
      nameSurname6: "",
      age6: "",
      occupation6: "",
      phoneNo6: "",
      nameSurname7: "",
      age7: "",
      occupation7: "",
      phoneNo7: "",
      nameSurname8: "",
      age8: "",
      occupation8: "",
      phoneNo8: "",
      nameSurname9: "",
      age9: "",
      occupation9: "",
      phoneNo9: "",
    },
    mode: "onTouched",
  });

  useEffect(() => {
    dispatch(personalActions.getInitial());
    dispatch(personalActions.getMilitary());
    dispatch(personalActions.getMaterial());
    dispatch(personalActions.getProvince());
    dispatch(personalActions.getPersonal());
    dispatch(personalActions.getAddress());
    dispatch(personalActions.getFamily());
    dispatch(personalActions.getStatus());
  }, []);

  useEffect(() => {
    if (personalReducer.resultPersonal) {
      setStartDate(Moment(personalReducer.resultPersonal.dateOfIssue).toDate());
      setEndDate(Moment(personalReducer.resultPersonal.expired).toDate());
      setStartDate2(
        Moment(personalReducer.resultPersonal.passportStart).toDate()
      );
      setEndDate2(Moment(personalReducer.resultPersonal.passportExp).toDate());
      setDiffday(
        Moment().diff(
          Moment(personalReducer.resultPersonal.birthday).toDate(),
          "years"
        )
      );
      if (personalReducer.resultPersonal.initialId == 4) {
        setOtherInitial(false);
        setValidateInitial(true);
      }
      if (personalReducer.resultPersonal.militaryStatusID == 6) {
        setisMilitary(false);
        setValidateOther(true);
      }
      if (personalReducer.resultPersonal.maritalId == 2) {
        setisMarried(false);
        setValidateMarried(true);
      }
      if (personalReducer.resultPersonal.noOfChildren) {
        setChildren(parseInt(personalReducer.resultPersonal.noOfChildren));
        setBoy(parseInt(personalReducer.resultPersonal.noOfBoys));
        setGirl(parseInt(personalReducer.resultPersonal.noOfGirls));
      }
      setTimeout(() => {
        reset({
          initialId: personalReducer.resultPersonal.initialId,
          initialOtherDetail: personalReducer.resultPersonal.initialOtherDetail,
          nameTh: personalReducer.resultPersonal.nameTh,
          surnameTh: personalReducer.resultPersonal.surnameTh,
          nicknameTh: personalReducer.resultPersonal.nicknameTh,
          nameEng: personalReducer.resultPersonal.nameEng,
          surnameEng: personalReducer.resultPersonal.surnameEng,
          birthday: personalReducer.resultPersonal.birthday
            ? Moment(personalReducer.resultPersonal.birthday).toDate()
            : null,
          height: personalReducer.resultPersonal.height,
          weight: personalReducer.resultPersonal.weight,
          bloodGroup: personalReducer.resultPersonal.bloodGroup,
          nationality: personalReducer.resultPersonal.nationality,
          religion: personalReducer.resultPersonal.religion,
          hometown: personalReducer.resultPersonal.hometown,
          idCard: personalReducer.resultPersonal.idCard,
          dateOfIssue: personalReducer.resultPersonal.dateOfIssue
            ? Moment(personalReducer.resultPersonal.dateOfIssue).toDate()
            : null,
          expired: personalReducer.resultPersonal.expired
            ? Moment(personalReducer.resultPersonal.expired).toDate()
            : null,
          issuedBy: personalReducer.resultPersonal.issuedBy,
          provincePersonal: personalReducer.resultPersonal.provincePersonal,
          passportNo: personalReducer.resultPersonal.passportNo,
          passportStart: personalReducer.resultPersonal.passportStart
            ? Moment(personalReducer.resultPersonal.passportStart).toDate()
            : null,
          passportExp: personalReducer.resultPersonal.passportExp
            ? Moment(personalReducer.resultPersonal.passportExp).toDate()
            : null,
          militaryStatusID: personalReducer.resultPersonal.militaryStatusID,
          militaryOtherStatus:
            personalReducer.resultPersonal.militaryOtherStatus,
          maritalId: personalReducer.resultPersonal.maritalId,
          name: personalReducer.resultPersonal.name,
          surname: personalReducer.resultPersonal.surname,
          age: personalReducer.resultPersonal.age,
          occupation: personalReducer.resultPersonal.occupation,
          company: personalReducer.resultPersonal.company,
          phoneNo: personalReducer.resultPersonal.phoneNo,
          noOfChildren: personalReducer.resultPersonal.noOfChildren,
          noOfBoys: personalReducer.resultPersonal.noOfBoys,
          noOfGirls: personalReducer.resultPersonal.noOfGirls,
        });
      }, 100);
    } else {
      // reset({
      //   initialId: null,
      //   initialOtherDetail: null,
      //   nameTh: null,
      //   surnameTh: null,
      //   nicknameTh: null,
      //   nameEng: null,
      //   surnameEng: null,
      //   birthday: null,
      //   height: null,
      //   weight: null,
      //   bloodGroup: null,
      //   nationality: null,
      //   religion: null,
      //   hometown: null,
      //   idCard: null,
      //   dateOfIssue: null,
      //   expired: null,
      //   issuedBy: null,
      //   provincePersonal: null,
      //   passportNo: null,
      //   passportStart: null,
      //   passportExp: null,
      //   militaryStatusID: null,
      //   militaryOtherStatus: null,
      //   maritalId: null,
      //   name: null,
      //   surname: null,
      //   age: null,
      //   occupation: null,
      //   company: null,
      //   phoneNo: null,
      //   noOfChildren: null,
      //   noOfBoys: null,
      //   noOfGirls: null,
      // });
    }
  }, [personalReducer.resultPersonal]);

  useEffect(() => {
    if (personalReducer.resultAddress) {
      setTimeout(() => {
        reset2({
          houseNo: personalReducer.resultAddress.houseNo,
          villageNo: personalReducer.resultAddress.villageNo,
          building: personalReducer.resultAddress.building,
          roomNo: personalReducer.resultAddress.roomNo,
          floor: personalReducer.resultAddress.floor,
          village: personalReducer.resultAddress.village,
          lane: personalReducer.resultAddress.lane,
          road: personalReducer.resultAddress.road,
          subArea: personalReducer.resultAddress.subArea,
          area: personalReducer.resultAddress.area,
          province: personalReducer.resultAddress.province,
          zipCode: personalReducer.resultAddress.zipCode,
          phoneNo: personalReducer.resultAddress.phoneNo,
          ext: personalReducer.resultAddress.ext,
        });
      }, 100);
    }
  }, [personalReducer.resultAddress]);

  useEffect(() => {
    if (personalReducer.resultFamily) {
      setRelatives(personalReducer.resultFamily.noOfRelatives);
      setTimeout(() => {
        reset3(
          {
            fatherName: personalReducer.resultFamily.fatherName,
            fatherSurname: personalReducer.resultFamily.fatherSurname,
            ageF: personalReducer.resultFamily.ageF,
            statusF: personalReducer.resultFamily.statusF,
            occupationF: personalReducer.resultFamily.occupationF,
            phoneNoF: personalReducer.resultFamily.phoneNoF,
            motherName: personalReducer.resultFamily.motherName,
            motherSurname: personalReducer.resultFamily.motherSurname,
            ageM: personalReducer.resultFamily.ageM,
            statusM: personalReducer.resultFamily.statusM,
            occupationM: personalReducer.resultFamily.occupationM,
            phoneNoM: personalReducer.resultFamily.phoneNoM,
            noOfRelatives: personalReducer.resultFamily.noOfRelatives,

            nameSurname1: personalReducer.resultFamily.nameSurname1,
            age1: personalReducer.resultFamily.age1,
            occupation1: personalReducer.resultFamily.occupation1,
            phoneNo1: personalReducer.resultFamily.phoneNo1,
            no1: personalReducer.resultFamily.no1,
            nameSurname2: personalReducer.resultFamily.nameSurname2,
            age2: personalReducer.resultFamily.age2,
            occupation2: personalReducer.resultFamily.occupation2,
            phoneNo2: personalReducer.resultFamily.phoneNo2,
            no2: personalReducer.resultFamily.no2,
            nameSurname3: personalReducer.resultFamily.nameSurname3,
            age3: personalReducer.resultFamily.age3,
            occupation3: personalReducer.resultFamily.occupation3,
            phoneNo3: personalReducer.resultFamily.phoneNo3,
            no3: personalReducer.resultFamily.no3,
            nameSurname4: personalReducer.resultFamily.nameSurname4,
            age4: personalReducer.resultFamily.age4,
            occupation4: personalReducer.resultFamily.occupation4,
            phoneNo4: personalReducer.resultFamily.phoneNo4,
            no4: personalReducer.resultFamily.no4,
            nameSurname5: personalReducer.resultFamily.nameSurname5,
            age5: personalReducer.resultFamily.age5,
            occupation5: personalReducer.resultFamily.occupation5,
            phoneNo5: personalReducer.resultFamily.phoneNo5,
            no5: personalReducer.resultFamily.no5,
            nameSurname6: personalReducer.resultFamily.nameSurname6,
            age6: personalReducer.resultFamily.age6,
            occupation6: personalReducer.resultFamily.occupation6,
            phoneNo6: personalReducer.resultFamily.phoneNo6,
            no6: personalReducer.resultFamily.no6,
            nameSurname7: personalReducer.resultFamily.nameSurname7,
            age7: personalReducer.resultFamily.age7,
            occupation7: personalReducer.resultFamily.occupation7,
            phoneNo7: personalReducer.resultFamily.phoneNo7,
            no7: personalReducer.resultFamily.no7,
            nameSurname8: personalReducer.resultFamily.nameSurname8,
            age8: personalReducer.resultFamily.age8,
            occupation8: personalReducer.resultFamily.occupation8,
            phoneNo8: personalReducer.resultFamily.phoneNo8,
            no8: personalReducer.resultFamily.no8,
            nameSurname9: personalReducer.resultFamily.nameSurname9,
            age9: personalReducer.resultFamily.age9,
            occupation9: personalReducer.resultFamily.occupation9,
            phoneNo9: personalReducer.resultFamily.phoneNo9,
            no9: personalReducer.resultFamily.no9,
          },
          100
        );
      });
    }
  }, [personalReducer.resultFamily]);

  const useRelatives = (count) => {
    for (var i = 0; i < count; i++) {
      rowRelatives.push(addRelatives(i));
    }
  };
  const addRelatives = (i) => {
    return (
      <div className="col-lg-12 row">
        <div className="col-lg-3">
          <div className="form-group">
            <p className="pull-left">
              {i + 1}.ชื่อ-นามสกุล (Name-Surname) :
              <span className="text-danger">&nbsp;*</span>
            </p>

            <input
              className="form-control"
              type="text"
              name={`nameSurname${i + 1}`}
              maxLength={50}
              ref={register3}
            />
            {errors3[`nameSurname${i + 1}`] && (
              <p style={styles.error}>
                {errors3[`nameSurname${i + 1}`].message}
              </p>
            )}
          </div>
        </div>{" "}
        <div className="col-lg-3">
          <div className="form-group">
            <p className="pull-left">
              อายุ/ปี (Age/Yrs.) :<span className="text-danger">&nbsp;*</span>
            </p>

            <input
              className="form-control"
              type="text"
              name={`age${i + 1}`}
              maxLength={2}
              ref={register3}
            />
            {errors3[`age${i + 1}`] && (
              <p style={styles.error}>{errors3[`age${i + 1}`].message}</p>
            )}
          </div>
        </div>{" "}
        <div className="col-lg-3">
          <div className="form-group">
            <p className="pull-left">
              อาชีพ (Occupation) :<span className="text-danger">&nbsp;*</span>
            </p>

            <input
              className="form-control"
              type="text"
              name={`occupation${i + 1}`}
              maxLength={50}
              ref={register3}
            />
            {errors3[`occupation${i + 1}`] && (
              <p style={styles.error}>
                {errors3[`occupation${i + 1}`].message}
              </p>
            )}
          </div>
        </div>
        <div className="col-lg-3">
          <div className="form-group">
            <p className="pull-left">
              หมายเลขโทรศัพท์ (Tel.No.) :
              <span className="text-danger">&nbsp;*</span>
            </p>

            <input
              className="form-control"
              type="text"
              name={`phoneNo${i + 1}`}
              maxLength={10}
              ref={register3}
            />
            {errors3[`phoneNo${i + 1}`] && (
              <p style={styles.error}>{errors3[`phoneNo${i + 1}`].message}</p>
            )}
          </div>
        </div>
      </div>
    );
  };
  const calChildren = (value) => {
    if (value >= 0) {
      setChildren(value);
      setBoy(value);
      setGirl(0);
    } else {
      setChildren(0);
    }
  };
  const calChildren2 = (value) => {
    let total = parseInt(children) || 0;
    if (value >= total) {
      setBoy(total);
      setGirl(0);
    }
    if (value < total) {
      if (value >= 0) {
        let cur = 0;
        cur = total - value;
        setBoy(total - cur);
        setGirl(cur);
      }
    }
  };

  const calChildren3 = (value) => {
    let total = parseInt(children) || 0;
    if (value >= total) {
      setGirl(total);
      setBoy(0);
    }
    if (value < total) {
      if (value >= 0) {
        let cur = 0;
        cur = total - value;
        setGirl(total - cur);
        setBoy(cur);
      }
    }
  };
  const handleClickNav1 = () => {
    setNav1(!nav1);
  };
  const handleClickNav2 = () => {
    setNav2(!nav2);
  };
  const handleClickNav3 = () => {
    setNav3(!nav3);
  };

  const onSubmit = (data) => {
    dispatch(personalActions.updatePersonal(data));
  };
  const onSubmit2 = (data) => dispatch(personalActions.updateAddress(data));
  const onSubmit3 = (data) => dispatch(personalActions.updateFamily(data));
  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      // subheader={
      //   <ListSubheader component="div" id="nested-list-subheader">
      //     ข้อมูลส่วนตัว (Personal Details)
      //   </ListSubheader>
      // }
      className={classes.root}
    >
      <form>
        {/* Nav1 */}
        <ListItem button onClick={handleClickNav1}>
          <ListItemText primary="ข้อมูลส่วนตัว (Personal Details)" />
          {nav1 ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={nav1} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <div button className={classes.nested}>
              {/* <ScrollToTop smooth /> */}
              <div className="col-lg-12 row">
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      คำนำหน้าชื่อ (<strong>Initial</strong>) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <select
                      className="form-control"
                      name="initialId"
                      ref={register}
                      onChange={(e) =>
                        e.target.value == 4
                          ? (setOtherInitial(false), setValidateInitial(true))
                          : (setOtherInitial(true),
                            setInitialOtherValues(""),
                            setValidateInitial(false))
                      }
                    >
                      <option value>-- โปรดเลือก / Please Select --</option>
                      {personalReducer.resultInitial
                        ? personalReducer.resultInitial.map((resultInitial) => (
                            <option value={resultInitial.InitialId}>
                              {resultInitial.InitialName}
                            </option>
                          ))
                        : []}
                    </select>
                    {errors.initialId && (
                      <p style={styles.error}>{errors.initialId.message}</p>
                    )}
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      โปรดระบุ (<strong>Please Spectify</strong>) :
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="initialOtherDetail"
                      maxLength={50}
                      readOnly={otherInitial}
                      onChange={(e) => setInitialOtherValues(e.target.value)}
                      ref={register}
                      value={values}
                    />
                    {errors.initialOtherDetail && validateInitial && (
                      <p style={styles.error}>
                        {errors.initialOtherDetail.message}
                      </p>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      ชื่อภาษาไทย :<span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="nameTh"
                      maxLength={50}
                      ref={register}
                    />
                    {errors.nameTh && (
                      <p style={styles.error}>{errors.nameTh.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      นามสกุล :<span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="surnameTh"
                      maxLength={50}
                      ref={register}
                    />
                    {errors.surnameTh && (
                      <p style={styles.error}>{errors.surnameTh.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      ชื่อเล่น :<span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="nicknameTh"
                      maxLength={50}
                      ref={register}
                    />
                    {errors.nicknameTh && (
                      <p style={styles.error}>{errors.nicknameTh.message}</p>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      Name in English :
                      <span className="text-danger">&nbsp;*</span>
                    </p>

                    <input
                      className="form-control"
                      type="text"
                      name="nameEng"
                      maxLength={50}
                      ref={register}
                    />
                    {errors.nameEng && (
                      <p style={styles.error}>{errors.nameEng.message}</p>
                    )}
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      Surname in English :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="surnameEng"
                      maxLength={50}
                      ref={register}
                    />
                    {errors.surnameEng && (
                      <p style={styles.error}>{errors.surnameEng.message}</p>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                <div className="col-lg-4">
                  <div className="form-group">
                    <div style={{ marginBottom: "10px" }}>
                      <p>
                        วันเกิด (<strong>Birthday</strong>) :
                        <span className="text-danger">&nbsp;*</span>
                      </p>
                    </div>
                    <div className="form-group">
                      <Controller
                        name="birthday"
                        control={control}
                        render={({ onChange, value }) => (
                          <ReactDatePicker
                            className="form-control"
                            onChange={(date) => {
                              onChange(date); // เปลี่ยนค่าของ name availableDate
                              setDiffday(Moment().diff(Moment(date), "years"));
                            }}
                            selected={value} //เปลี่ยนข้อความตามวันที่่เลือก
                            placeholderText="DD/MM/YYYY"
                            dateFormat="dd/MM/yyyy"
                            autoComplete="off"
                            peekNextMonth
                            showMonthDropdown
                            showYearDropdown
                            dropdownMode="select"
                          />
                        )}
                      />
                      {errors.birthday && (
                        <p style={styles.error}>{errors.birthday.message}</p>
                      )}
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="form-group">
                    <p>
                      {" "}
                      อายุ/ปี (<strong>Age/Yrs.</strong>) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="age"
                      readOnly="readonly"
                      value={diffday}
                    />
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      น้ำหนัก/ก.ก. (Weight/Kg.) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="weight"
                      maxLength={50}
                      ref={register}
                    />
                    {errors.weight && (
                      <p style={styles.error}>{errors.weight.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      ส่วนสูง/ซ.ม.(Height/Cm.) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="height"
                      maxLength={50}
                      ref={register}
                    />
                    {errors.height && (
                      <p style={styles.error}>{errors.height.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      หมู่โลหิต (Blood Group) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="bloodGroup"
                      maxLength={50}
                      ref={register}
                    />
                    {errors.bloodGroup && (
                      <p style={styles.error}>{errors.bloodGroup.message}</p>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      สัญชาติ (Nationality) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="nationality"
                      maxLength={50}
                      ref={register}
                    />
                    {errors.nationality && (
                      <p style={styles.error}>{errors.nationality.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      ศาสนา (Religion) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="religion"
                      maxLength={50}
                      ref={register}
                    />
                    {errors.religion && (
                      <p style={styles.error}>{errors.religion.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      จังหวัดที่เกิด (Hometown) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <select
                      className="form-control"
                      name="hometown"
                      ref={register}
                    >
                      <option value>-- โปรดเลือก / Please Select --</option>
                      {personalReducer.resultProvince
                        ? personalReducer.resultProvince.map(
                            (resultProvince) => (
                              <option value={resultProvince.ProvinceId}>
                                {resultProvince.ProvinceThai}
                              </option>
                            )
                          )
                        : []}
                    </select>
                    {errors.hometown && (
                      <p style={styles.error}>{errors.hometown.message}</p>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      เลขที่บัตรประชาชน (ID.Card.) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>

                    <input
                      className="form-control"
                      type="text"
                      name="idCard"
                      maxLength={13}
                      ref={register}
                    />
                    {errors.idCard && (
                      <p style={styles.error}>{errors.idCard.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      วันที่ออกบัตร-{">"}วันที่หมดอายุ (Date of issue/Expired)
                    </p>
                    <InputGroup>
                      <div className="row">
                        <div className="col" style={{ paddingRight: "0px" }}>
                          <Controller
                            name="dateOfIssue"
                            control={control}
                            render={({ onChange, value }) => (
                              <ReactDatePicker
                                className="form-control"
                                onChange={(date) => {
                                  onChange(date); // เปลี่ยนค่าของ name availableDate
                                  setStartDate(date);
                                }}
                                selected={value} //เปลี่ยนข้อความตามวันที่่เลือก
                                placeholderText="DD/MM/YYYY"
                                dateFormat="dd/MM/yyyy"
                                autoComplete="off"
                                peekNextMonth
                                showMonthDropdown
                                showYearDropdown
                                dropdownMode="select"
                                selectsStart
                                // startDate={startDate}
                                // endDate={endDate}
                              />
                            )}
                          />
                        </div>
                        <InputGroupText className="col-2">ถึง</InputGroupText>
                        <div className="col" style={{ paddingLeft: "0px" }}>
                          <Controller
                            name="expired"
                            control={control}
                            render={({ onChange, value }) => (
                              <ReactDatePicker
                                className="form-control"
                                onChange={(date) => {
                                  onChange(date); // เปลี่ยนค่าของ name availableDate
                                  setEndDate(date);
                                }}
                                selected={value} //เปลี่ยนข้อความตามวันที่่เลือก
                                placeholderText="DD/MM/YYYY"
                                dateFormat="dd/MM/yyyy"
                                autoComplete="off"
                                peekNextMonth
                                showMonthDropdown
                                showYearDropdown
                                dropdownMode="select"
                                selectsEnd
                                // startDate={startDate}
                                // endDate={endDate}
                                minDate={startDate}
                              />
                            )}
                          />
                        </div>
                      </div>
                    </InputGroup>
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      ออกให้โดย (Issued by) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="issuedBy"
                      maxLength={50}
                      ref={register}
                    />
                    {errors.issuedBy && (
                      <p style={styles.error}>{errors.issuedBy.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      จังหวัด (Province) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <select
                      className="form-control"
                      name="provincePersonal"
                      ref={register}
                    >
                      <option value>-- โปรดเลือก / Please Select --</option>
                      {personalReducer.resultProvince
                        ? personalReducer.resultProvince.map(
                            (resultProvince) => (
                              <option value={resultProvince.ProvinceId}>
                                {resultProvince.ProvinceThai}
                              </option>
                            )
                          )
                        : []}
                    </select>
                    {errors.provincePersonal && (
                      <p style={styles.error}>
                        {errors.provincePersonal.message}
                      </p>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      เลขที่หนังสือเดินทาง (Passport No.) :
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="passportNo"
                      maxLength={10}
                      ref={register}
                    />
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p
                      className="pull-left"
                      style={{ wordWrap: "break-word", wordBreak: "break-all" }}
                    >
                      วันที่ออกหนังสือ-{">"}วันที่หมดอายุ (Date of
                      issue/Expired)
                    </p>
                    <InputGroup>
                      <div className="row">
                        <div className="col" style={{ paddingRight: "0px" }}>
                          <Controller
                            name="passportStart"
                            control={control}
                            render={({ onChange, value }) => (
                              <ReactDatePicker
                                className="form-control"
                                onChange={(date) => {
                                  onChange(date); // เปลี่ยนค่าของ name availableDate
                                  setStartDate2(date);
                                }}
                                selected={value} //เปลี่ยนข้อความตามวันที่่เลือก
                                placeholderText="DD/MM/YYYY"
                                dateFormat="dd/MM/yyyy"
                                autoComplete="off"
                                peekNextMonth
                                showMonthDropdown
                                showYearDropdown
                                dropdownMode="select"
                                selectsStart
                                // startDate={startDate2}
                                // endDate={endDate2}
                              />
                            )}
                          />
                        </div>
                        <InputGroupText className="col-2">ถึง</InputGroupText>
                        <div className="col" style={{ paddingLeft: "0px" }}>
                          <Controller
                            name="passportExp"
                            control={control}
                            render={({ onChange, value }) => (
                              <ReactDatePicker
                                className="form-control"
                                onChange={(date) => {
                                  onChange(date); // เปลี่ยนค่าของ name availableDate
                                  setEndDate2(date);
                                }}
                                selected={value} //เปลี่ยนข้อความตามวันที่่เลือก
                                placeholderText="DD/MM/YYYY"
                                dateFormat="dd/MM/yyyy"
                                autoComplete="off"
                                peekNextMonth
                                showMonthDropdown
                                showYearDropdown
                                dropdownMode="select"
                                selectsEnd
                                // startDate={startDate2}
                                // endDate={endDate2}
                                minDate={startDate2}
                              />
                            )}
                          />
                        </div>
                      </div>
                    </InputGroup>
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      สถานภาพทางทหาร (Military Service) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <select
                      className="form-control"
                      name="militaryStatusID"
                      ref={register}
                      onChange={(e) =>
                        e.target.value == 6
                          ? (setisMilitary(false), setValidateOther(true))
                          : (setisMilitary(true),
                            setMilitaryOtherValues(""),
                            setValidateOther(false))
                      }
                    >
                      <option value>-- โปรดเลือก / Please Select --</option>
                      {personalReducer.resultMilitary
                        ? personalReducer.resultMilitary.map(
                            (resultMilitary) => (
                              <option value={resultMilitary.MilitaryId}>
                                {resultMilitary.MilitaryStatusName}
                              </option>
                            )
                          )
                        : []}
                    </select>
                    {errors.militaryStatusID && (
                      <p style={styles.error}>
                        {errors.militaryStatusID.message}
                      </p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      โปรดระบุ (Please Spectify) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="militaryOtherStatus"
                      maxLength={50}
                      onChange={(e) => setMilitaryOtherValues(e.target.value)}
                      ref={register}
                      readOnly={isMilitary}
                      value={militaryOther}
                    />
                    {errors.militaryOtherStatus && validateOther && (
                      <p style={styles.error}>
                        {errors.militaryOtherStatus.message}
                      </p>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      สถานภาพสมรส (Marital Status) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <select
                      className="form-control"
                      name="maritalId"
                      ref={register}
                      onChange={(e) =>
                        e.target.value == 2
                          ? (setisMarried(false), setValidateMarried(true))
                          : (setisMarried(true),
                            setMaritalOtherFname(""),
                            setMaritalOtherLname(""),
                            setMaritalOtherAge(""),
                            setMaritalOtherOccupation(""),
                            setMaritalOtherCompany(""),
                            setMaritalOtherPhoneNo(""),
                            setChildren(""),
                            setValidateMarried(false))
                      }
                    >
                      <option value>-- โปรดเลือก / Please Select --</option>
                      {personalReducer.resultMaterial
                        ? personalReducer.resultMaterial.map(
                            (resultMaterial) => (
                              <option value={resultMaterial.MaritalId}>
                                {resultMaterial.MaritalStatusName}
                              </option>
                            )
                          )
                        : []}
                    </select>
                    {errors.maritalId && (
                      <p style={styles.error}>{errors.maritalId.message}</p>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      ชื่อคู่สมรส (Spouse's Name) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="name"
                      maxLength={50}
                      onChange={(e) => setMaritalOtherFname(e.target.value)}
                      ref={register}
                      readOnly={isMarried}
                      value={maritalOtherFname}
                    />
                    {errors.name && validateMarried && (
                      <p style={styles.error}>{errors.name.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      นามสกุล (Spouse's Surname) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="surname"
                      maxLength={50}
                      onChange={(e) => setMaritalOtherLname(e.target.value)}
                      ref={register}
                      readOnly={isMarried}
                      value={maritalOtherLname}
                    />
                    {errors.surname && validateMarried && (
                      <p style={styles.error}>{errors.surname.message}</p>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      อายุ/ปี (Age/Yrs.) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="age"
                      maxLength={2}
                      onChange={(e) => setMaritalOtherAge(e.target.value)}
                      ref={register}
                      readOnly={isMarried}
                      value={maritalOtherAge}
                    />
                    {errors.age && validateMarried && (
                      <p style={styles.error}>{errors.age.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      อาชีพ (Occupation) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="occupation"
                      maxLength={50}
                      onChange={(e) =>
                        setMaritalOtherOccupation(e.target.value)
                      }
                      ref={register}
                      readOnly={isMarried}
                      value={maritalOtherOccupation}
                    />
                    {errors.occupation && validateMarried && (
                      <p style={styles.error}>{errors.occupation.message}</p>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      สถานที่ทำงาน (Company) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="company"
                      maxLength={50}
                      onChange={(e) => setMaritalOtherCompany(e.target.value)}
                      ref={register}
                      readOnly={isMarried}
                      value={maritalOtherCompany}
                    />
                    {errors.company && validateMarried && (
                      <p style={styles.error}>{errors.company.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      หมายเลขโทรศัพท์ (Tel. No.) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="phoneNo"
                      maxLength={10}
                      onChange={(e) => setMaritalOtherPhoneNo(e.target.value)}
                      ref={register}
                      readOnly={isMarried}
                      value={maritalOtherPhoneNo}
                    />
                    {errors.phoneNo && validateMarried && (
                      <p style={styles.error}>{errors.phoneNo.message}</p>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      จำนวนบุตร (No. of Children) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="number"
                      name="noOfChildren"
                      maxLength={2}
                      onChange={(event) => calChildren(event.target.value)}
                      ref={register}
                      readOnly={isMarried}
                      value={children}
                    />
                    {errors.noOfChildren && validateMarried && (
                      <p style={styles.error}>{errors.noOfChildren.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      เป็นชาย/คน (No. Of Boys/Peoples) :
                    </p>
                    <input
                      className="form-control"
                      type="number"
                      name="noOfBoys"
                      maxLength={2}
                      ref={register}
                      readOnly={isMarried}
                      onChange={(event) => calChildren2(event.target.value)}
                      value={boy}
                      // max="10"
                    />
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      เป็นหญิง/คน (No. Of Girls/Peoples) :
                    </p>
                    <input
                      className="form-control"
                      type="number"
                      name="noOfGirls"
                      maxLength={2}
                      ref={register}
                      readOnly={isMarried}
                      onChange={(event) => calChildren3(event.target.value)}
                      value={girl}
                      // max="10"
                    />
                  </div>
                </div>
              </div>
            </div>
          </List>
        </Collapse>
      </form>

      <form>
        {/* Nav2 */}
        <ListItem button onClick={handleClickNav2}>
          <ListItemText primary="ข้อมูลที่อยู่ปัจจุบัน (Current Address)" />
          {nav2 ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={nav2} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <div button className={classes.nested}>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      เลขที่ (House No.) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>

                    <input
                      className="form-control"
                      type="text"
                      name="houseNo"
                      maxLength={50}
                      ref={register2}
                    />
                    {errors2.houseNo && (
                      <p style={styles.error}>{errors2.houseNo.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">หมู่ที่ (Village No.) :</p>
                    <input
                      className="form-control"
                      type="text"
                      name="villageNo"
                      maxLength={50}
                      ref={register2}
                    />
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">อาคาร (Building) :</p>
                    <input
                      className="form-control"
                      type="text"
                      name="building"
                      maxLength={50}
                      ref={register2}
                    />
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">เลขที่ห้อง (Room No.) :</p>

                    <input
                      className="form-control"
                      type="text"
                      name="roomNo"
                      maxLength={50}
                      ref={register2}
                    />
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">ชั้นที่ (Floor) :</p>

                    <input
                      className="form-control"
                      type="text"
                      name="floor"
                      maxLength={50}
                      ref={register2}
                    />
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">หมู่บ้าน (Village) :</p>
                    <input
                      className="form-control"
                      type="text"
                      name="village"
                      maxLength={50}
                      ref={register2}
                    />
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">ตรอก/ซอย (Lane/Alley) :</p>

                    <input
                      className="form-control"
                      type="text"
                      name="lane"
                      maxLength={50}
                      ref={register2}
                    />
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      ถนน (Road) :<span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="road"
                      maxLength={50}
                      ref={register2}
                    />
                    {errors2.road && (
                      <p style={styles.error}>{errors2.road.message}</p>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      ตำบล/แขวง (Sub-District/Sub-Area) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>

                    <input
                      className="form-control"
                      type="text"
                      name="subArea"
                      maxLength={50}
                      ref={register2}
                    />
                    {errors2.subArea && (
                      <p style={styles.error}>{errors2.subArea.message}</p>
                    )}
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      อำเภอ/เขต (District/Area) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="area"
                      maxLength={50}
                      ref={register2}
                    />
                    {errors2.area && (
                      <p style={styles.error}>{errors2.area.message}</p>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      จังหวัด (province) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <select
                      className="form-control"
                      name="province"
                      ref={register2}
                    >
                      <option value>-- โปรดเลือก / Please Select --</option>
                      {personalReducer.resultProvince
                        ? personalReducer.resultProvince.map(
                            (resultProvince) => (
                              <option value={resultProvince.ProvinceId}>
                                {resultProvince.ProvinceThai}
                              </option>
                            )
                          )
                        : []}
                    </select>
                    {errors2.province && (
                      <p style={styles.error}>{errors2.province.message}</p>
                    )}
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      รหัสไปษณีย์ (Zip Code) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="zipCode"
                      maxLength={5}
                      ref={register2}
                    />
                    {errors2.zipCode && (
                      <p style={styles.error}>{errors2.zipCode.message}</p>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      หมายเลขโทรศัพท์ที่พัก (Tel.No.) :
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="phoneNo"
                      maxLength={10}
                      ref={register2}
                      // onChange={(event) => setPhoneNoExt(event.target.value)}
                      // value={phoneNoExt}
                    />
                    {errors2.phoneNo && (
                      <p style={styles.error}>{errors2.phoneNo.message}</p>
                    )}
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">ต่อ (ext.) :</p>
                    <input
                      className="form-control"
                      type="text"
                      name="ext"
                      maxLength={50}
                      ref={register2}
                    />
                  </div>
                </div>
              </div>
            </div>
          </List>
        </Collapse>
      </form>

      <form>
        {/* Nav3 */}
        <ListItem button onClick={handleClickNav3}>
          <ListItemText primary="ข้อมูลเกี่ยวกับครอบครัว (Family Background)" />
          {nav3 ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={nav3} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <div button className={classes.nested}>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      ชื่อบิดา (Father's Name) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>

                    <input
                      className="form-control"
                      type="text"
                      name="fatherName"
                      maxLength={50}
                      ref={register3}
                    />
                    {errors3.fatherName && (
                      <p style={styles.error}>{errors3.fatherName.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      นามสกุลบิดา (Father's Surname) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>

                    <input
                      className="form-control"
                      type="text"
                      name="fatherSurname"
                      maxLength={50}
                      ref={register3}
                    />
                    {errors3.fatherSurname && (
                      <p style={styles.error}>
                        {errors3.fatherSurname.message}
                      </p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      อายุ/ปี (Age/Yrs.) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>

                    <input
                      className="form-control"
                      type="text"
                      name="ageF"
                      maxLength={2}
                      ref={register3}
                    />
                    {errors3.ageF && (
                      <p style={styles.error}>{errors3.ageF.message}</p>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      สถานะ (Status) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <select
                      className="form-control"
                      name="statusF"
                      ref={register3}
                    >
                      <option value="" selected>
                        -- โปรดเลือก / Please Select --
                      </option>
                      {personalReducer.resultStatus
                        ? personalReducer.resultStatus.map((resultStatus) => (
                            <option value={resultStatus.statusId}>
                              {resultStatus.statusName}
                            </option>
                          ))
                        : []}
                    </select>
                    {errors3.statusF && (
                      <p style={styles.error}>{errors3.statusF.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      อาชีพบิดา (Occupation) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>

                    <input
                      className="form-control"
                      type="text"
                      name="occupationF"
                      maxLength={50}
                      ref={register3}
                    />
                    {errors3.occupationF && (
                      <p style={styles.error}>{errors3.occupationF.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      หมายเลขโทรศัพท์ (Tel. No.) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>

                    <input
                      className="form-control"
                      type="text"
                      name="phoneNoF"
                      maxLength={10}
                      ref={register3}
                    />
                    {errors3.phoneNoF && (
                      <p style={styles.error}>{errors3.phoneNoF.message}</p>
                    )}
                  </div>
                </div>
              </div>{" "}
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      ชื่อมารดา (Mother's Name) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="motherName"
                      maxLength={50}
                      ref={register3}
                    />
                    {errors3.motherName && (
                      <p style={styles.error}>{errors3.motherName.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      นามสกุลมารดา (Mother's Surname) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>

                    <input
                      className="form-control"
                      type="text"
                      name="motherSurname"
                      maxLength={50}
                      ref={register3}
                    />
                    {errors3.motherSurname && (
                      <p style={styles.error}>
                        {errors3.motherSurname.message}
                      </p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      อายุ/ปี (Age/Yrs.) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>

                    <input
                      className="form-control"
                      type="text"
                      name="ageM"
                      maxLength={2}
                      ref={register3}
                    />
                    {errors3.ageM && (
                      <p style={styles.error}>{errors3.ageM.message}</p>
                    )}
                  </div>
                </div>
              </div>{" "}
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      สถานะ (Status) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <select
                      className="form-control"
                      name="statusM"
                      ref={register3}
                    >
                      <option value="" selected>
                        -- โปรดเลือก / Please Select --
                      </option>
                      {personalReducer.resultStatus
                        ? personalReducer.resultStatus.map((resultStatus) => (
                            <option value={resultStatus.statusId}>
                              {resultStatus.statusName}
                            </option>
                          ))
                        : []}
                    </select>
                    {errors3.statusM && (
                      <p style={styles.error}>{errors3.statusM.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      อาชีพมารดา (Occupation) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="occupationM"
                      maxLength={50}
                      ref={register3}
                    />
                    {errors3.occupationM && (
                      <p style={styles.error}>{errors3.occupationM.message}</p>
                    )}
                  </div>
                </div>{" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      หมายเลขโทรศัพท์ (Tel. No.) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>
                    <input
                      className="form-control"
                      type="text"
                      name="phoneNoM"
                      maxLength={10}
                      ref={register3}
                    />
                    {errors3.phoneNoM && (
                      <p style={styles.error}>{errors3.phoneNoM.message}</p>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-lg-12 row">
                {" "}
                <div className="col-lg-4">
                  <div className="form-group">
                    <p className="pull-left">
                      จำนวนพี่น้องไม่รวมท่าน (No. of Relatives) :
                      <span className="text-danger">&nbsp;*</span>
                    </p>

                    <input
                      className="form-control"
                      type="text"
                      name="noOfRelatives"
                      maxLength={1}
                      onChange={(event) => setRelatives(event.target.value)}
                      ref={register3}
                    />
                    {errors3.noOfRelatives && (
                      <p style={styles.error}>
                        {errors3.noOfRelatives.message}
                      </p>
                    )}
                  </div>
                </div>
              </div>
              {useRelatives(Relatives)}
              {rowRelatives.map((element) => {
                return element;
              })}
            </div>
          </List>
        </Collapse>
      </form>

      <button
        className="btn btn-w-m btn-warning"
        onClick={() => {
          handleSubmit(onSubmit)();
          handleSubmit2(onSubmit2)();
          handleSubmit3(onSubmit3)();
        }}
      >
        Submit
      </button>
    </List>
  );
}
