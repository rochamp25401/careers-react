import React from "react";
import { shallow } from "enzyme";
import LanguageSkillPage from "./LanguageSkillPage";

describe("LanguageSkillPage", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<LanguageSkillPage />);
    expect(wrapper).toMatchSnapshot();
  });
});
