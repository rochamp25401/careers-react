import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import * as loginActions from "./../../../actions/login.action";
import "./LoginPage.css";

const styles = {
  error: {
    color: "#bf1650",
  },
};

const schema = yup.object().shape({
  email: yup.string().required(),
  password: yup
    .string()
    .min(3, "Please Enter less then 3 letters")
    .required("This field is required."),
});

export default function LoginPage(props) {
  const dispatch = useDispatch();
  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
    mode: "onTouched",
  });
  const onSubmit = (data) => {
    dispatch(loginActions.login(data, props.history));
  };

  return (
    <div className="container-sm" style={{ width: "350px" }}>
      <div className="pace  pace-inactive">
        <div
          className="pace-progress"
          data-progress-text="100%"
          data-progress={99}
          style={{ transform: "translate3d(100%, 0px, 0px)" }}
        >
          <div className="pace-progress-inner" />
        </div>
        <div className="pace-activity" />
      </div>

      <div className="middle-box text-center loginscreen">
        <div>
          <div>
            <img
              style={{ width: 300, height: 75 }}
              alt="image"
              className="img-responsive"
              src={require("./../../../img/iconext.png").default}
            />
          </div>
          <form className="m-t" onSubmit={handleSubmit(onSubmit)}>
            <br />
            <br />
            <div className="form-group ">
              <span className="h4">
                <small>
                  <a className="text-warning text-bold" href>
                    Candidate
                  </a>
                </small>
              </span>{" "}
              |{" "}
              <span className="h4">
                <small>
                  <a className="" href="../careers/staff.php">
                    Staff iCONEXT
                  </a>
                </small>
              </span>
            </div>
            <div className="form-group">
              <input
                type="email"
                placeholder="E-mail"
                className={`form-control ${errors.email ? "is-invalid" : ""}`}
                name="email"
                id="email"
                ref={register}
              />
              {errors.email && (
                <p style={styles.error}>{errors.email.message}</p>
              )}
            </div>
            <div className="form-group">
              <input
                type="password"
                placeholder="Password"
                className={`form-control ${
                  errors.password ? "is-invalid" : ""
                }`}
                name="password"
                id="password"
                ref={register}
              />
              {errors.password && (
                <p style={styles.error}>{errors.password.message}</p>
              )}
            </div>
            <button
              className="btn full-width"
              style={{
                backgroundColor: "#ff7f26",
                borderColor: "#ff7f26",
                color: "#fff",
                marginBottom: "15px",
              }}
              data-style="zoom-out"
              src="image/login_button.png"
              name="Submit"
              type="submit"
            >
              Login
            </button>
          </form>
          <div className="form-group ">
            <a
              className="h6"
              href="/register"
              style={{ fontSize: "14px", color: "gray" }}
            >
              Create Account
            </a>{" "}
            |{" "}
            <a
              className="text-gray h5"
              href="../careers/forget_password.php"
              style={{ fontSize: "14px", color: "gray" }}
            >
              Forgot Password
            </a>
          </div>
          <div className="m-t-xl">
            <p className="m-t">
              <img
                src={require("./../../../img/chome.png").default}
                style={{ width: 25, height: 25 }}
              />{" "}
              <small>currently supports the Chrome browser only</small>{" "}
            </p>
            <p className="m-t">
              {" "}
              <small>iCONEXT CO.,LTD. © 2017</small>{" "}
            </p>
            <p className="m-t">
              {" "}
              <small>
                121/30, RS Tower Building, 7th Floor, Ratchadapisek Road,
                Dindaeng, Bangkok 10400 THAILAND
              </small>{" "}
            </p>{" "}
          </div>
        </div>
      </div>
    </div>
  );
}
