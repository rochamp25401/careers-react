import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

import { makeStyles } from "@material-ui/core/styles";
import ListSubheader from "@material-ui/core/ListSubheader";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import DraftsIcon from "@material-ui/icons/Drafts";
import SendIcon from "@material-ui/icons/Send";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import StarBorder from "@material-ui/icons/StarBorder";
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#fff",
    fontSize: "12px",
    fontFamily:
      "Kanit, OpenSans, Helvetica Neue, Helvetica Arial, sans-serif !important",
  },
  nested: {
    paddingLeft: theme.spacing(5),
  },
}));

export default function ExperiencePage() {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [nav, setNav] = useState(true);
  // const [Relatives, setRelatives] = useState(0);
  // var rowRelatives = [];
  const handleClickNav = () => {
    setNav(!nav);
  };

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      // subheader={
      //   <ListSubheader component="div" id="nested-list-subheader">
      //     ข้อมูลส่วนตัว (Personal Details)
      //   </ListSubheader>
      // }
      className={classes.root}
    >
      <form>
        {/* Nav1 */}
        <ListItem button onClick={handleClickNav}>
          <ListItemText primary="ข้อมูลการทำงาน (Work Experience)" />
          {nav ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={nav} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {/* <div button className={classes.nested}> */}
            {/* {useRelatives(Relatives)}
              {rowRelatives.map((element) => {
                return element;
              })} */}
            {/* </div> */}
            <div className="ibox-content">
              <div className>
                <table id="tbExpExperience" className="table" border={0}>
                  <tbody>
                    <tr className="darkgray-bg">
                      <th style={{ width: "148px" }}>
                        <div className="form-group">
                          <p>ตั้งแต่ เดือน/ปี ค.ศ.</p>
                          <p>(Since MM/YYYY A.D.)</p>
                        </div>
                      </th>
                      <th style={{ width: "148px" }}>
                        <div className="form-group">
                          <p>ถึง เดือน/ปี ค.ศ.</p>
                          <p>(To MM/YYYY A.D.)</p>
                        </div>
                      </th>
                      <th>
                        <div className="form-group">
                          <p>ชื่อบริษัทฯ/องค์กร</p>
                          <p>(Company/ Organization)</p>
                        </div>
                      </th>
                      <th>
                        <div className="form-group">
                          <p>ตำแหน่ง</p>
                          <p>(Position)</p>
                        </div>
                      </th>
                      <th style={{ width: "110px" }}>
                        <div className="form-group">
                          <p>เงินเดือนล่าสุด</p>
                          <p>(Current Salary)</p>
                        </div>
                      </th>
                      <th>
                        <div className="form-group">
                          <p>สาเหตุที่ออก</p>
                          <p>(Reason of Leavingpfont)</p>
                        </div>
                      </th>
                    </tr>
                    <tr>
                      <td className="form-group">
                        <div className="input-group date">
                          <span className="input-group-addon">
                            <i className="fa fa-calendar" />
                          </span>
                          <input
                            className="form-control"
                            type="text"
                            name="Column1_1"
                            placeholder="MM/YYYY"
                          />
                        </div>
                      </td>
                      <td className="form-group">
                        <div className="input-group date">
                          <span className="input-group-addon">
                            <i className="fa fa-calendar" />
                          </span>
                          <input
                            className="form-control"
                            type="text"
                            name="Column2_1"
                            placeholder="MM/YYYY"
                          />
                        </div>
                      </td>
                      <td className="css-name">
                        <input
                          className="form-control"
                          type="text"
                          size={18}
                          maxLength={200}
                          name="Column3_1"
                          placeholder="Company/ Organization"
                        />
                      </td>
                      <td className="css-name">
                        <input
                          className="form-control"
                          type="text"
                          size={9}
                          maxLength={100}
                          name="Column4_1"
                          placeholder="Position"
                        />
                      </td>
                      <td className="css-name">
                        <div className="input-group bootstrap-touchspin">
                          <span
                            className="input-group-addon bootstrap-touchspin-prefix"
                            style={{ display: "none" }}
                          />
                          <input
                            type="text"
                            className="form-control touchspinSalary"
                            maxLength={6}
                            name="Column5_1"
                            placeholder="Current Salary"
                            style={{ display: "block" }}
                          />
                        </div>
                      </td>
                      <td className="css-name">
                        <input
                          className="form-control"
                          type="text"
                          size={14}
                          maxLength={500}
                          name="Column6_1"
                          placeholder="Reason of Leaving"
                        />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-lg-12">
                <p className="btn btn-danger fa fa-minus pull-right" ><AddIcon/></p>
                <p className="btn btn-primary fa fa-plus pull-right" ><RemoveIcon/></p>
              </div>
            </div>
          </List>
        </Collapse>
      </form>
      <button className="btn btn-w-m btn-warning" onClick={() => {}}>
        Submit
      </button>
    </List>
  );
}
