import React from "react";
import { shallow } from "enzyme";
import ExperiencePage from "./ExperiencePage";

describe("ExperiencePage", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<ExperiencePage />);
    expect(wrapper).toMatchSnapshot();
  });
});
