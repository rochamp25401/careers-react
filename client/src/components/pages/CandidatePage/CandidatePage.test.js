import React from "react";
import { shallow } from "enzyme";
import CandidatePage from "./CandidatePage";

describe("CandidatePage", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<CandidatePage />);
    expect(wrapper).toMatchSnapshot();
  });
});
