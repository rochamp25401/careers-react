import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useForm, Controller } from "react-hook-form";
import ReactDatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Moment from "moment";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import Button from "@material-ui/core/Button";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Box from "@material-ui/core/Box";
import PersonIcon from "@material-ui/icons/Person";
import SchoolIcon from "@material-ui/icons/School";
import BusinessCenterIcon from "@material-ui/icons/BusinessCenter";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrophy } from "@fortawesome/free-solid-svg-icons";
import ComputerIcon from "@material-ui/icons/Computer";
import TranslateIcon from "@material-ui/icons/Translate";
import SettingsIcon from "@material-ui/icons/Settings";
import InfoIcon from "@material-ui/icons/Info";
import AttachFileIcon from "@material-ui/icons/AttachFile";
import PersonalDetailsPage from "./../PersonalDetailsPage/PersonalDetailsPage";
import EducationPage from "./../EducationPage/EducationPage";
import ExperiencePage from "./../ExperiencePage/ExperiencePage";
import TrainingPage from "./../TrainingPage/TrainingPage";
import ComputerSkillPage from "./../ComputerSkillPage/ComputerSkillPage";
import LanguageSkillPage from "./../LanguageSkillPage/LanguageSkillPage";
import TypingPage from "./../TypingPage/TypingPage";
import OtherInformationPage from "./../OtherInformationPage/OtherInformationPage";
import FilePage from "./../FilePage/FilePage";
import * as candidateActions from "./../../../actions/candidate.action";
import * as loginActions from "./../../../actions/login.action";
import { useHistory } from "react-router-dom";
import {NotificationContainer, NotificationManager} from 'react-notifications';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {                       
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  tab: {
    minWidth: 100, // a number of your choice
    width: 119, // a number of your choice
  },
}));

const styles = {
  error: {
    color: "#bf1650",
  },
};

// const schema = yup.object().shape({
//   candidateId: yup.string(),
//   position: yup.string(),
//   availableDate: yup.string(),
//   email: yup.string(),
//   expectedSalary: yup.string(),
//   phoneNo: yup.string(),
// });

export default function CandidatePage(props) {
  const history = useHistory();
  const dispatch = useDispatch();
  const candidateReducer = useSelector(
    ({ candidateReducer }) => candidateReducer
  );
  const { loginReducer } = useSelector((state) => state);

  const { register, handleSubmit, errors, control, setValue, reset } = useForm({
    // reValidateMode: "onBlur",
    // resolver: yupResolver(schema),
  });
  const classes = useStyles();
  const [value, setValueTab] = useState(0);
  // console.log("errors", errors);

  useEffect(() => {
    dispatch(candidateActions.getPosition());
    dispatch(candidateActions.getCandidate());
    dispatch(candidateActions.getUser());
  }, [loginReducer]);

  useEffect(() => {
    if (candidateReducer.resultcandidate) {
      reset({
        position: candidateReducer.resultcandidate.position,
        expectedSalary: candidateReducer.resultcandidate.expectedSalary,
        availableDate: candidateReducer.resultcandidate.availableDate
          ? Moment(candidateReducer.resultcandidate.availableDate).toDate()
          : null,
        email: candidateReducer.resultcandidate.email,
        phoneNo: candidateReducer.resultcandidate.phoneNo,
      });
    }
  }, [candidateReducer.resultcandidate]);

  const onChangePosition = (data) => {
    dispatch(candidateActions.updateCandidate({ position: data.position }));
  };
  const onChangeAvailableDate = (data) => {
    dispatch(
      candidateActions.updateCandidate({ availableDate: data.availableDate })
    );
  };
  const onChangeEmail = (data) => {
    if (
      data.email.match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      )
    ) {
      dispatch(candidateActions.updateCandidate({ email: data.email }));
    }
  };
  const onChangeExpectedSalary = (data) => {
    if (
      data.expectedSalary.length <= 6 &&
      data.expectedSalary.match(/^[0-9]+$/) != null
    ) {
      dispatch(
        candidateActions.updateCandidate({
          expectedSalary: data.expectedSalary,
        })
      );
    }
  };
  const onChangePhoneNumber = (data) => {
    if (data.phoneNo.length == 10 && data.phoneNo.match(/^[0-9]+$/) != null) {
      dispatch(candidateActions.updateCandidate({ phoneNo: data.phoneNo }));
    }
  };

  const handleChange = (event, newValue) => {
    setValueTab(newValue);
  };

  return (
    <div>
      <NotificationContainer/>
      {/* <CssBaseline /> */}
      {/* in Container 1st */}
      <Container style={{ width: "1150px", padding: "0" }}>
        {/* Header */}
        <div style={{ backgroundColor: "#f3f3f4", height: "15vh" }}>
          <img
            style={{ width: 200, height: 50,margin: "10px" }}
            alt="image"
            className="img-responsive"
            src={require("./../../../img/iconext.png").default}
          />
          <Toolbar style={{ float: "right"}}>
            <p className="nav navbar-top-links navbar-right">
              Hi,
              {candidateReducer.resultgetUser
                ? candidateReducer.resultgetUser.userFirstname
                : "null"}{" "}
              {candidateReducer.resultgetUser
                ? candidateReducer.resultgetUser.userLastname
                : "null"}
            </p>
            {loginActions.isLoggedIn() ? (
              <Button
                color="inherit"
                onClick={() => {
                  dispatch(loginActions.logout());
                  history.push("/");
                }}
              >
                Logout
              </Button>
            ) : (
              <Button
                color="inherit"
                onClick={() => {
                  history.push("/login");
                }}
              >
                Login
              </Button>
            )}
          </Toolbar>
        </div>
        <div
          style={{
            backgroundColor: "#fff",
            paddingTop: "35px",
          }}
        >
          {/* in Container 2nd */}
          <Container style={{ width: "1100px", padding: "0" }}>
            {/* Label Application Form */}
            <div
              className="col-lg-12"
              style={{
                height: "50px",
                backgroundColor: "#FF7F26",
                padding: "15px 15px 7px 15px",
                marginBottom: "20px",
                color: "#fff",
              }}
            >
              <h5>Application Form</h5>
            </div>
            {/* blox แรก */}
            <div
              className="col-lg-12 row"
              style={{
                height: "322px",
              }}
            >
              <div
                className="col-lg-9"
                style={{
                  height: "273px",
                }}
              >
                <form>
                  <div className="col-lg-12 row">
                    <div className="col-lg-6 form-group">
                      <p>
                        ตำแหน่งที่สมัคร (<strong>Position Applied For</strong>)
                        :<i className="text-danger">&nbsp;*</i>
                      </p>
                      <select
                        name="position"
                        className="form-control"
                        ref={register}
                        onChange={handleSubmit(onChangePosition)}
                      >
                        <option value="" selected>
                          -- โปรดเลือก / Please Select --
                        </option>
                        {/* map drop down */}
                        {candidateReducer.resultpositions
                          ? candidateReducer.resultpositions.map(
                              (resultpositions) => (
                                <option value={resultpositions.PositionId}>
                                  {resultpositions.PositionName}
                                </option>
                              )
                            )
                          : []}
                      </select>
                    </div>
                  </div>
                  <div className="col-lg-12 row">
                    <div className="col-lg-6 form-group">
                      <p>
                        เงินเดือนที่คาดหวัง (<strong>Expected Salary</strong>) :
                        <i className="text-danger">&nbsp;*</i>
                      </p>
                      <input
                        className="form-control "
                        type="text"
                        name="expectedSalary"
                        maxLength={6}
                        ref={register({
                          pattern: {
                            value: /^[0-9]+$/,
                            message: "Number error",
                          },
                        })}
                        onChange={handleSubmit(onChangeExpectedSalary)}
                      />
                      {errors.expectedSalary && (
                        <p style={styles.error}>
                          {errors.expectedSalary.message}
                        </p>
                      )}
                    </div>
                    <div className="col-lg-6 form-group">
                      <div>
                        <p>
                          วันที่เริ่มงานได้ (<strong>Available Date</strong>) :
                          <i className="text-danger">&nbsp;*</i>
                        </p>
                      </div>
                      {/* <div className="form-group">
                        <div className="input-group date">
                          <span className="input-group-addon">
                            <i className="fa fa-calendar" />
                          </span>
                          <input
                            className="form-control bg-white-readonly"
                            type="date"
                            name="availableDate"
                            placeholder="DD/MM/YYYY"
                            ref={register}
                            onChange={handleSubmit(onChangeAvailableDate)}
                          />
                        </div>
                      </div> */}
                      <Controller
                        name="availableDate"
                        control={control}
                        render={({ onChange, value }) => (
                          <ReactDatePicker
                            className="form-control"
                            onChange={(date) => {
                              handleSubmit(
                                onChangeAvailableDate({ availableDate: date })
                              );
                              onChange(date); // เปลี่ยนค่าของ name availableDate
                            }}
                            selected={value} //เปลี่ยนข้อความตามวันที่่เลือก
                            placeholderText="DD/MM/YYYY"
                            dateFormat="dd/MM/yyyy"
                            autoComplete="off"
                            peekNextMonth
                            showMonthDropdown
                            showYearDropdown
                            dropdownMode="select"
                          />
                        )}
                      />
                    </div>
                  </div>
                  <div className="col-lg-12 row">
                    <div className="col-lg-6">
                      <div className="form-group">
                        <p>
                          อีเมล์ (<strong>E-mail Address</strong>) :
                          <i className="text-danger">&nbsp;*</i>
                        </p>
                        <input
                          className="form-control"
                          maxLength={50}
                          type="email"
                          name="email"
                          ref={register({
                            pattern: {
                              value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                              message: "Email error",
                            },
                          })}
                          onChange={handleSubmit(onChangeEmail)}
                        />
                        {errors.email && (
                          <p style={styles.error}>{errors.email.message}</p>
                        )}
                      </div>
                    </div>
                    <div className="col-lg-6">
                      <div className="form-group">
                        <p className="pull-left">
                          เบอร์ติดต่อ (<strong>Cell Phone No.</strong>) :
                          <i className="text-danger">&nbsp;*</i>
                        </p>
                        <input
                          className="form-control"
                          maxLength={10}
                          type="text"
                          name="phoneNo"
                          ref={register({
                            pattern: {
                              value: /^[0-9]+$/,
                              message: "PhoneNo error",
                            },
                          })}
                          onChange={handleSubmit(onChangePhoneNumber)}
                        />
                        {errors.phoneNo && (
                          <p style={styles.error}>{errors.phoneNo.message}</p>
                        )}
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              {/* รูปถ่ายประจำตัว */}
              <div
                className="col-lg-3"
                style={{
                  width: "200px",
                  height: "250px",
                  textAlign: "center",
                  verticalAlign: "middle",
                  display: "block",
                }}
              >
                <img
                  style={{
                    verticalAlign: "middle",
                    width: "100%",
                    height: "100%",
                  }}
                  src={
                    require("./../../../img/dummy-profile-pic-300x300-1.png")
                      .default
                  }
                />
              </div>
              <div className="col-lg-12 row">
                <div className="col-lg-9"></div>
                <div className="col-lg-3">
                  <button
                    type="button"
                    className="btn btn-warning btn-block m-b ladda-button"
                    style={{
                      marginLeft: "25px",
                      backgroundColor: "#FF7F26",
                      borderColor: "#FF7F26",
                      color: "#FFFFFF",
                    }}
                  >
                    <i className="fa fa-paper-plane" aria-hidden="true" />
                    <span> Apply Job</span>
                  </button>
                </div>
              </div>
            </div>

            {/* Tab */}
            <div className={(classes.root, "col-lg-12")}>
              <AppBar position="static">
                <Tabs
                  value={value}
                  onChange={handleChange}
                  aria-label="simple tabs example"
                >
                  <Tab
                    classes={{ root: classes.tab }}
                    label={<PersonIcon />}
                    {...a11yProps(0)}
                  />
                  <Tab
                    classes={{ root: classes.tab }}
                    label={<SchoolIcon />}
                    {...a11yProps(1)}
                  />
                  <Tab
                    classes={{ root: classes.tab }}
                    label={<BusinessCenterIcon />}
                    {...a11yProps(2)}
                  />
                  <Tab
                    classes={{ root: classes.tab }}
                    label={<FontAwesomeIcon icon={faTrophy} />}
                    {...a11yProps(3)}
                  />
                  <Tab
                    classes={{ root: classes.tab }}
                    label={<ComputerIcon />}
                    {...a11yProps(4)}
                  />
                  <Tab
                    classes={{ root: classes.tab }}
                    label={<TranslateIcon />}
                    {...a11yProps(5)}
                  />
                  <Tab
                    classes={{ root: classes.tab }}
                    label={<SettingsIcon />}
                    {...a11yProps(6)}
                  />
                  <Tab
                    classes={{ root: classes.tab }}
                    label={<InfoIcon />}
                    {...a11yProps(7)}
                  />
                  <Tab
                    classes={{ root: classes.tab }}
                    label={<AttachFileIcon />}
                    {...a11yProps(8)}
                  />
                </Tabs>
              </AppBar>
              <TabPanel value={value} index={0}>
                <PersonalDetailsPage />
              </TabPanel>
              <TabPanel value={value} index={1}>
                <EducationPage />
              </TabPanel>
              <TabPanel value={value} index={2}>
                <ExperiencePage />
              </TabPanel>
              <TabPanel value={value} index={3}>
                <TrainingPage />
              </TabPanel>
              <TabPanel value={value} index={4}>
                <ComputerSkillPage />
              </TabPanel>
              <TabPanel value={value} index={5}>
                <LanguageSkillPage />
              </TabPanel>
              <TabPanel value={value} index={6}>
                <TypingPage />
              </TabPanel>
              <TabPanel value={value} index={7}>
                <OtherInformationPage />
              </TabPanel>
              <TabPanel value={value} index={8}>
                <FilePage />
              </TabPanel>
            </div>
          </Container>
        </div>
      </Container>
    </div>
  );
}
