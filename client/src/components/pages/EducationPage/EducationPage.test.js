import React from "react";
import { shallow } from "enzyme";
import EducationPage from "./EducationPage";

describe("EducationPage", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<EducationPage />);
    expect(wrapper).toMatchSnapshot();
  });
});
