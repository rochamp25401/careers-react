import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

import { makeStyles } from "@material-ui/core/styles";
import ListSubheader from "@material-ui/core/ListSubheader";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import DraftsIcon from "@material-ui/icons/Drafts";
import SendIcon from "@material-ui/icons/Send";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import StarBorder from "@material-ui/icons/StarBorder";
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#fff",
    fontSize: "12px",
    fontFamily:
      "Kanit, OpenSans, Helvetica Neue, Helvetica Arial, sans-serif !important",
  },
  nested: {
    paddingLeft: theme.spacing(5),
  },
}));
export default function EducationPage() {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [nav, setNav] = useState(true);
  // const [Relatives, setRelatives] = useState(0);
  // var rowRelatives = [];
  const handleClickNav = () => {
    setNav(!nav);
  };

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      // subheader={
      //   <ListSubheader component="div" id="nested-list-subheader">
      //     ข้อมูลส่วนตัว (Personal Details)
      //   </ListSubheader>
      // }
      className={classes.root}
    >
      <form>
        {/* Nav1 */}
        <ListItem button onClick={handleClickNav}>
          <ListItemText primary="ประวัติการศึกษา (Education Background)" />
          {nav ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={nav} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {/* <div button className={classes.nested}> */}
            {/* {useRelatives(Relatives)}
              {rowRelatives.map((element) => {
                return element;
              })} */}
            {/* </div> */}
            <div className="ibox-content">
              <table id="tbExpEducation" className="table" border={0}>
                <tbody>
                  <tr className="darkgray-bg" align="center">
                    <td width="30%" colSpan={2}>
                      <strong>
                        <label>&nbsp;ปีที่เข้าศึกษา</label>
                      </strong>
                      <br />
                      <strong>
                        <label>(Year Attended)</label>
                      </strong>
                    </td>
                    <td width="15%" rowSpan={2}>
                      <strong>
                        <label>&nbsp;วุฒิการศึกษา</label>
                      </strong>
                      <br />
                      <strong>
                        <label>(Degree)</label>
                      </strong>
                    </td>
                    <td width="25%" rowSpan={2}>
                      <strong>
                        <label>&nbsp;ชื่อสถานศึกษา</label>
                      </strong>
                      <br />
                      <strong>
                        <label>(Name of School/College/University)</label>
                      </strong>
                    </td>
                    <td width="20%" rowSpan={2}>
                      <strong>
                        <label>&nbsp;สาขาวิชา/คณะ</label>
                      </strong>
                      <br />
                      <strong>
                        <label>(Major/Faculty)</label>
                      </strong>
                    </td>
                    <td width="10%" rowSpan={2}>
                      <strong>
                        <label>&nbsp;เกรดเฉลี่ย</label>
                      </strong>
                      <br />
                      <strong>
                        <label>(GPA)</label>
                      </strong>
                    </td>
                  </tr>
                  <tr className="darkgray-bg" height="30px">
                    <td width="15%" align="center">
                      <strong>
                        <label>&nbsp;ตั้งแต่ เดือน/ปี ค.ศ.</label>
                      </strong>
                      <br />
                      <strong>
                        <label>(Since MM/YYYY A.D.)</label>
                      </strong>
                    </td>
                    <td width="15%" align="center">
                      <strong>
                        <label>&nbsp;ถึง เดือน/ปี ค.ศ.</label>
                      </strong>
                      <br />
                      <strong>
                        <label>(To MM/YYYY A.D.)</label>
                      </strong>
                    </td>
                  </tr>
                  <tr>
                    <td align="center" className="form-group">
                      <div className="input-group date">
                        <span className="input-group-addon">
                          <i className="fa fa-calendar" />
                        </span>
                        <input
                          className="form-control"
                          type="text"
                          name="Column1_2"
                          placeholder="MM/YYYY"
                        />
                      </div>
                    </td>
                    <td align="center" className="form-group">
                      <div className="input-group date">
                        <span className="input-group-addon">
                          <i className="fa fa-calendar" />
                        </span>
                        <input
                          className="form-control"
                          type="text"
                          name="Column2_2"
                          placeholder="MM/YYYY"
                        />
                      </div>
                    </td>
                    <td align="center">
                      <input
                        className="form-control"
                        type="text"
                        size={8}
                        maxLength={100}
                        name="Column3_2"
                        placeholder="Degree"
                      />
                    </td>
                    <td align="center">
                      <input
                        className="form-control"
                        type="text"
                        size={17}
                        maxLength={100}
                        name="Column4_2"
                        placeholder="Name of School/College/University"
                      />
                    </td>
                    <td align="center">
                      <input
                        className="form-control"
                        type="text"
                        size={13}
                        maxLength={100}
                        name="Column5_2"
                        placeholder="Major/Faculty"
                      />
                    </td>
                    <td align="center">
                      <input
                        className="form-control"
                        type="text"
                        name="Column6_2"
                        placeholder="X.XX"
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
              <div className="col-lg-12">
                <p className="btn btn-danger fa fa-minus pull-right" ><AddIcon/></p>
                <p className="btn btn-primary fa fa-plus pull-right" ><RemoveIcon/></p>
              </div>
              <div className="col-lg-12">
                <font
                  style={{
                    fontWeight: "bold",
                    fontSize: "12px",
                    paddingLeft: "5px",
                  }}
                >
                  กิจกรรมนอกหลักสูตร หรือกิจกรรมระหว่างศึกษา
                </font>
                <br />
                <font
                  style={{
                    fontWeight: "bold",
                    fontSize: "12px",
                    paddingLeft: "5px",
                  }}
                >
                  (Extra-Curricular Activities, Academic Activities or
                  Recreation Activities)
                </font>
                <br />
                <textarea
                  style={{ width: "890px" }}
                  name="txtActivities"
                  className="form-control"
                  maxLength={9999}
                />
              </div>
            </div>
          </List>
        </Collapse>
      </form>
      <button className="btn btn-w-m btn-warning" onClick={() => {}}>
        Submit
      </button>
    </List>
  );
}
