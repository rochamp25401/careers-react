import React from "react";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import * as registerActions from "./../../../actions/register.action";

const styles = {
  error: {
    color: "#bf1650",
  },
};

const schema = yup.object().shape({
  firstname: yup.string().required("FirstName is a required field"),
  lastname: yup.string().required("LastName is a required field"),
  email: yup.string().required("Email is a required field"),
  password: yup
    .string()
    .min(3, "Please enter less than 3 letters")
    .required("This field is required."),
  confirmPassword: yup
    .string()
    .min(3, "Please enter less than 3 letters")
    .required("This field is required.")
    //check is password match ?
    .test("passwords-match", "Password not match........", function (value) {
      return this.parent.password === value;
    }),
});

export default function RegisterPage(props) {
  const history = useHistory();
  const dispatch = useDispatch();
  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
    mode: "onTouched",
  });
  const onSubmit = (data) => {
    dispatch(registerActions.register(data, props.history));
    console.log(data);
  };
  return (
    <>
      <div
        className="container col-lg-12"
        style={{
          width: "450px",
          marginTop: 30,
          paddingTop: 20,
          backgroundColor: "#fff",
        }}
      >
        <div className="col-lg-12 m-t-md">
          <div>
            <form
              className="form-inline"
              role="form"
              onSubmit={handleSubmit(onSubmit)}
            >
              <div className="animated fadeInDown">
                <div className="ibox-content">
                  <span className="font-bold h4 text-muted">Register</span>
                  <p className="text-muted">
                    Enter your information to register.
                  </p>
                  <div>
                    <div className="col-lg-12 m-t-sm">
                      <div className="form-group">
                        <input
                          type="text"
                          className={`form-control ${
                            errors.firstname ? "is-invalid" : ""
                          }`}
                          id="firstname"
                          name="firstname"
                          placeholder="FirstName"
                          style={{ width: "350px", marginBottom: 10 }}
                          ref={register}
                        />
                        <span className="text-danger">*</span>
                        {errors.firstname && (
                          <p style={styles.error}>{errors.firstname.message}</p>
                        )}
                      </div>
                    </div>
                    <div className="col-lg-12">
                      <div className="form-group m-t-sm">
                        <input
                          type="text"
                          className={`form-control ${
                            errors.lastname ? "is-invalid" : ""
                          }`}
                          id="lastname"
                          name="lastname"
                          placeholder="LastName"
                          style={{ width: "350px", marginBottom: 10 }}
                          ref={register}
                        />
                        <span className="text-danger">*</span>
                        {errors.lastname && (
                          <p style={styles.error}>{errors.lastname.message}</p>
                        )}
                      </div>
                    </div>
                    <div className="col-lg-12">
                      <div className="form-group m-t-sm">
                        <input
                          type="email"
                          className={`form-control ${
                            errors.email ? "is-invalid" : ""
                          }`}
                          id="email"
                          name="email"
                          placeholder="Email"
                          style={{ width: "350px", marginBottom: 10 }}
                          ref={register}
                        />
                        <span className="text-danger">*</span>
                        {errors.email && (
                          <p style={styles.error}>{errors.email.message}</p>
                        )}
                      </div>
                    </div>
                    <div className="col-lg-12">
                      <div className="form-group m-t-sm">
                        <input
                          type="password"
                          className={`form-control ${
                            errors.password ? "is-invalid" : ""
                          }`}
                          id="password"
                          name="password"
                          placeholder="Password"
                          style={{ width: "350px", marginBottom: 10 }}
                          ref={register}
                        />
                        <span className="text-danger">*</span>
                        {errors.password && (
                          <p style={styles.error}>{errors.password.message}</p>
                        )}
                      </div>
                    </div>
                    <div className="col-lg-12">
                      <div className="form-group m-t-sm">
                        <input
                          id="confirmPassword"
                          className={`form-control ${
                            errors.confirmPassword ? "is-invalid" : ""
                          }`}
                          name="confirmPassword"
                          type="password"
                          placeholder="ConfirmPassword"
                          style={{ width: "350px", marginBottom: 10 }}
                          ref={register}
                        />
                        <span className="text-danger">*</span>
                        {errors.confirmPassword && (
                          <p style={styles.error}>
                            {errors.confirmPassword.message}
                          </p>
                        )}
                      </div>
                    </div>
                    <div
                      className="container text-center"
                      style={{ margin: "5px 0px 15px 0px" }}
                    >
                      <button className="btn btn-w-m btn-warning" type="submit">
                        Submit
                      </button>{" "}
                      <button
                        className="btn btn-outline-danger"
                        onClick={() => {
                          history.push("/");
                        }}
                      >
                        Cancel
                      </button>
                      <br />
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div className="col-lg-4" />
        </div>
      </div>
      <footer
        className="col-lg-12 text-center"
        style={{
          position: "relative",
          bottom: 0,
          left: 0,
          right: 0,
        }}
      >
        <br />
        <p>
          <small>iCONEXT CO.,LTD. © 2017</small>
        </p>
        <p>
          <small>
            121/30, RS Tower Building, 7th Floor, Ratchadapisek Road, Dindaeng,
            Bangkok 10400
          </small>{" "}
        </p>
        <p>
          <small />{" "}
        </p>
      </footer>
    </>
  );
}
