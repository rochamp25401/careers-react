import React from "react";
import { shallow } from "enzyme";
import FilePage from "./FilePage";

describe("FilePage", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<FilePage />);
    expect(wrapper).toMatchSnapshot();
  });
});
