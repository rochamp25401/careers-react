import React from "react";
import { shallow } from "enzyme";
import TrainingPage from "./TrainingPage";

describe("TrainingPage", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<TrainingPage />);
    expect(wrapper).toMatchSnapshot();
  });
});
