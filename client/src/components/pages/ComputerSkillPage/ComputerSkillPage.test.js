import React from "react";
import { shallow } from "enzyme";
import ComputerSkillPage from "./ComputerSkillPage";

describe("ComputerSkillPage", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<ComputerSkillPage />);
    expect(wrapper).toMatchSnapshot();
  });
});
