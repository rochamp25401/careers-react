import React from "react";
import { shallow } from "enzyme";
import OtherInformationPage from "./OtherInformationPage";

describe("OtherInformationPage", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<OtherInformationPage />);
    expect(wrapper).toMatchSnapshot();
  });
});
