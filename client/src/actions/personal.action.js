import {
  PERSONAL_FETCHING,
  PERSONAL_SUCCESS,
  PERSONAL_FAILED,
  ADDRESS_FETCHING,
  ADDRESS_SUCCESS,
  ADDRESS_FAILED,
  FAMILY_FETCHING,
  FAMILY_SUCCESS,
  FAMILY_FAILED,
  INITIAL_FETCHING,
  INITIAL_SUCCESS,
  INITIAL_FAILED,
  MILITARY_FETCHING,
  MILITARY_SUCCESS,
  MILITARY_FAILED,
  MARITAL_FETCHING,
  MARITAL_SUCCESS,
  MARITAL_FAILED,
  PROVINCE_FETCHING,
  PROVINCE_SUCCESS,
  PROVINCE_FAILED,
  STATUS_FETCHING,
  STATUS_SUCCESS,
  STATUS_FAILED,
  server,
} from "../constants";
import { httpClient } from "./../utils/HttpClient";
import Swal from "sweetalert2";

//-------------------------------Personal--------------------------------------
export const setPersonalStateToFetch = () => ({
  type: PERSONAL_FETCHING,
});

export const setPersonalStateToSuccess = (payload) => ({
  type: PERSONAL_SUCCESS,
  payload,
});

export const setPersonalStatetoFailed = () => ({
  type: PERSONAL_FAILED,
});
//-------------------------------Address--------------------------------------
export const setAddressStateToFetch = () => ({
  type: ADDRESS_FETCHING,
});

export const setAddressStateToSuccess = (payload) => ({
  type: ADDRESS_SUCCESS,
  payload,
});

export const setAddressStatetoFailed = () => ({
  type: ADDRESS_FAILED,
});
//-------------------------------Family--------------------------------------
export const setFamilyStateToFetch = () => ({
  type: FAMILY_FETCHING,
});

export const setFamilyStateToSuccess = (payload) => ({
  type: FAMILY_SUCCESS,
  payload,
});

export const setFamilyStatetoFailed = () => ({
  type: FAMILY_FAILED,
});

//-------------------------------Initial--------------------------------------
export const setInitialStateToFetch = () => ({
  type: INITIAL_FETCHING,
});

export const setInitialStateToSuccess = (payload) => ({
  type: INITIAL_SUCCESS,
  payload,
});

export const setInitialStatetoFailed = () => ({
  type: INITIAL_FAILED,
});
//-------------------------------Military--------------------------------------
export const setMilitaryStateToFetch = () => ({
  type: MILITARY_FETCHING,
});

export const setMilitaryStateToSuccess = (payload) => ({
  type: MILITARY_SUCCESS,
  payload,
});

export const setMilitaryStatetoFailed = () => ({
  type: MILITARY_FAILED,
});
//-------------------------------Material--------------------------------------
export const setMaterialStateToFetch = () => ({
  type: MARITAL_FETCHING,
});

export const setMaterialStateToSuccess = (payload) => ({
  type: MARITAL_SUCCESS,
  payload,
});

export const setMaterialStatetoFailed = () => ({
  type: MARITAL_FAILED,
});
//-------------------------------Province--------------------------------------
export const setProvinceStateToFetch = () => ({
  type: PROVINCE_FETCHING,
});

export const setProvinceStateToSuccess = (payload) => ({
  type: PROVINCE_SUCCESS,
  payload,
});

export const setProvinceStatetoFailed = () => ({
  type: PROVINCE_FAILED,
});

//-------------------------------Status--------------------------------------
export const setStatusStateToFetch = () => ({
  type: STATUS_FETCHING,
});

export const setStatusStateToSuccess = (payload) => ({
  type: STATUS_SUCCESS,
  payload,
});

export const setStatusStatetoFailed = () => ({
  type: STATUS_FAILED,
});

//------------------------------------------------------------------------------
const updateSuccress = (value1, value2, value3) => {
  if (value1 && value2 && value3) {
    // window.scrollTo({ top: 0, behavior: "smooth" });
    Swal.fire("Success", "Your register is success", "success").then(
      setTimeout(() => {
        window.location.reload();
      }, 700)
    );
    result1 = false;
    result2 = false;
    result3 = false;
  }
};

let result1 = false;
let result2 = false;
let result3 = false;
export const updatePersonal = (value) => {
  return async (dispatch) => {
    console.log(value);
    dispatch(setPersonalStateToFetch());
    const result = await httpClient.put(server.PERSONAL_URL, value);
    console.log(result);
    if (result.data.result === "ok") {
      result1 = true;
      updateSuccress(result1, result2, result3);
      // dispatch(setPersonalStateToSuccess(result.data));
    } else {
      dispatch(setPersonalStatetoFailed());
    }
  };
};

export const updateAddress = (value) => {
  return async (dispatch) => {
    // console.log(value)
    dispatch(setAddressStateToFetch());
    const result = await httpClient.put(server.ADDRESS_URL, value);
    console.log(result);
    if (result.data.result === "ok") {
      result2 = true;
      updateSuccress(result1, result2, result3);
      // dispatch(setAddressStateToSuccess(result.data));
    } else {
      dispatch(setAddressStatetoFailed());
    }
  };
};

export const updateFamily = (value) => {
  return async (dispatch) => {
    // console.log(value)
    dispatch(setAddressStateToFetch());
    const result = await httpClient.put(server.FAMILY_URL, value);
    console.log(result);
    if (result.data.result === "ok") {
      result3 = true;
      updateSuccress(result1, result2, result3);
      // dispatch(setAddressStateToSuccess(result.data));
    } else {
      dispatch(setAddressStatetoFailed());
    }
  };
};

export const getPersonal = () => {
  return async (dispatch) => {
    dispatch(setPersonalStateToFetch());
    const result = await httpClient.get(server.GETPERSONAL_URL);
    if (result) {
      dispatch(setPersonalStateToSuccess(result.data));
    } else {
      dispatch(setPersonalStatetoFailed());
    }
  };
};
export const getAddress = () => {
  return async (dispatch) => {
    dispatch(setAddressStateToFetch());
    const result = await httpClient.get(server.GETADDRESS_URL);
    if (result) {
      dispatch(setAddressStateToSuccess(result.data));
    } else {
      dispatch(setAddressStatetoFailed());
    }
  };
};
export const getFamily = () => {
  return async (dispatch) => {
    dispatch(setFamilyStateToFetch());
    const result = await httpClient.get(server.GETFAMILY_URL);
    if (result) {
      dispatch(setFamilyStateToSuccess(result.data));
    } else {
      dispatch(setFamilyStatetoFailed());
    }
  };
};

export const getInitial = () => {
  return async (dispatch) => {
    dispatch(setInitialStateToFetch());
    const result = await httpClient.get(server.INITIAL_URL);
    if (result) {
      dispatch(setInitialStateToSuccess(result.data));
    } else {
      dispatch(setInitialStatetoFailed());
    }
  };
};

export const getMilitary = (value) => {
  return async (dispatch) => {
    dispatch(setMilitaryStateToFetch());
    const result = await httpClient.get(server.MILITARY_URL, value);
    if (result) {
      dispatch(setMilitaryStateToSuccess(result.data));
    } else {
      dispatch(setMilitaryStatetoFailed());
    }
  };
};

export const getMaterial = (value) => {
  return async (dispatch) => {
    dispatch(setMaterialStateToFetch());
    const result = await httpClient.get(server.MARTIAL_URL, value);
    if (result) {
      dispatch(setMaterialStateToSuccess(result.data));
    } else {
      dispatch(setMaterialStatetoFailed());
    }
  };
};

export const getProvince = (value) => {
  return async (dispatch) => {
    dispatch(setProvinceStateToFetch());
    const result = await httpClient.get(server.PROVINCE_URL, value);
    if (result) {
      dispatch(setProvinceStateToSuccess(result.data));
    } else {
      dispatch(setProvinceStatetoFailed());
    }
  };
};

export const getStatus = (value) => {
  return async (dispatch) => {
    dispatch(setStatusStateToFetch());
    const result = await httpClient.get(server.STATUS_URL, value);
    if (result) {
      dispatch(setStatusStateToSuccess(result.data));
    } else {
      dispatch(setStatusStatetoFailed());
    }
  };
};
