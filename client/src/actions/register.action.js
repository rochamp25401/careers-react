import {
  REGISTER_FETCHING,
  REGISTER_SUCCESS,
  REGISTER_FAILED,
} from "./../constants";

import axios from "axios";
import Swal from "sweetalert2";

export const setRegisterStateToFetch = () => ({
  type: REGISTER_FETCHING,
});

export const setRegisterStateToSuccess = (payload) => ({
  type: REGISTER_SUCCESS,
  payload,
});

export const setRegisterStateToFailed = (payload) => ({
  type: REGISTER_FAILED,
  payload,
});

export const register = (account, history) => {
  return async (dispatch) => {
    try {
      dispatch(setRegisterStateToFetch());
      const result = await axios.post(
        "http://localhost:8080/api/register",
        account
      );
      if (result.data.result === "ok") {
        dispatch(setRegisterStateToSuccess(result.data));
        Swal.fire("Success", "Your register is success", "success");
        history.push("/login");
      } else {
        dispatch(setRegisterStateToFailed({ result: "register error" }));
        Swal.fire({
          icon: "error",
          title: "Error",
          text: "This Email has been registered!",
        });
      }
    } catch (e) {
      dispatch(setRegisterStateToFailed({ result: JSON.stringify(e) }));
    }
  };
};
