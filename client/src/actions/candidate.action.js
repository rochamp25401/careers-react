import {
  POSITION_FETCHING,
  POSITION_SUCCESS,
  POSITION_FAILED,
  CANDIDATE_FETCHING,
  CANDIDATE_SUCCESS,
  CANDIDATE_FAILED,
  GETUSER_URL,
  GETUSER_FETCHING,
  GETUSER_SUCCESS,
  GETUSER_FAILED,
  server,
} from "../constants";

import { httpClient } from "./../utils/HttpClient";
import {NotificationManager} from 'react-notifications';
//-------------------------------Position--------------------------------------
export const setPostitonStateToFetch = () => ({
  type: POSITION_FETCHING,
});

export const setPostitonStateToSuccess = (payload) => ({
  type: POSITION_SUCCESS,
  payload,
});

export const setPostitonStatetoFailed = () => ({
  type: POSITION_FAILED,
});
//-------------------------------Candidate--------------------------------------
export const setCandidateStateToFetch = () => ({
  type: CANDIDATE_FETCHING,
});

export const setCandidateStateToSuccess = (payload) => ({
  type: CANDIDATE_SUCCESS,
  payload,
});

export const setCandidateStatetoFailed = () => ({
  type: CANDIDATE_FAILED,
});
//-------------------------------getUser--------------------------------------
export const setGetUserStateToFetch = () => ({
  type: GETUSER_FETCHING,
});

export const setGetUserStateToSuccess = (payload) => ({
  type: GETUSER_SUCCESS,
  payload,
});

export const setGetUserStatetoFailed = () => ({
  type: GETUSER_FAILED,
});

export const updateCandidate = (value) => {
  return async (dispatch) => {
    // console.log(value)
    dispatch(setCandidateStateToFetch());
    const result = await httpClient.put(server.CANDIDATE_URL, value);
    console.log(result);
    if (result.data.result === "ok") {
      // NotificationManager.success('Success message', 'Success',);
    } else {
      dispatch(setCandidateStatetoFailed());
    }
  };
};

export const getCandidate = () => {
  return async (dispatch) => {
    try {
      dispatch(setCandidateStateToFetch());
      const result = await httpClient.get(server.CANDIDATE_URL);
      if (result) {
        dispatch(setCandidateStateToSuccess(result.data));
      }
    } catch (e) {
      dispatch(setCandidateStatetoFailed());
    }
  };
};

export const getPosition = () => {
  return async (dispatch) => {
    dispatch(setPostitonStateToFetch());
    const result = await httpClient.get(server.POSITION_URL);
    if (result) {
      dispatch(setPostitonStateToSuccess(result.data));
    } else {
      dispatch(setPostitonStatetoFailed());
    }
  };
};

export const getUser = () => {
  return async (dispatch) => {
    try {
      dispatch(setGetUserStateToFetch());
      const result = await httpClient.get(server.GETUSER_URL);
      if (result) {
        dispatch(setGetUserStateToSuccess(result.data));
      } else {
        console.log("result is null");
      }
    } catch (e) {
      dispatch(setGetUserStatetoFailed());
    }
  };
};
