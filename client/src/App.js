import React from "react";
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";
import LoginPage from "./components/pages/LoginPage";
import RegisterPage from "./components/pages/RegisterPage";
import CandidatePage from "./components/pages/CandidatePage";
import PersonalDetailsPage from "./components/pages/PersonalDetailsPage";
import 'react-notifications/lib/notifications.css';


function App() {
  return (
    <Router>
      <Switch>
        <Route path="/register" component={RegisterPage} />
        <Route path="/login" component={LoginPage} />
        <Route path="/candidate" component={CandidatePage} />
        <Route path="/detail" component={PersonalDetailsPage} />
        <Route path="/" component={() => <Redirect to="/login" />} />
      </Switch>
    </Router>
  );
} 

export default App;
