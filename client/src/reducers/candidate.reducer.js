import {
  POSITION_FETCHING,
  POSITION_SUCCESS,
  POSITION_FAILED,
  CANDIDATE_FETCHING,
  CANDIDATE_SUCCESS,
  CANDIDATE_FAILED,
  GETUSER_FETCHING,
  GETUSER_SUCCESS,
  GETUSER_FAILED,
} from "../constants";

const initialState = {
  resultpositions: null,
  resultcandidate: null,
  resultgetUser: null,
  isFetching: false,
  isError: false,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    //candidate
    case CANDIDATE_FETCHING:
      return { ...state, isFetching: true, isError: false, resultcandidate: null };
    case CANDIDATE_SUCCESS:
      return { ...state, isFetching: false, isError: false, resultcandidate: payload };
    case CANDIDATE_FAILED:
      return { ...state, isFetching: false, isError: true, resultcandidate: null };

    //positions
    case POSITION_FETCHING:
      return { ...state, isFetching: true, isError: false, resultpositions: null };
    case POSITION_SUCCESS:
      return { ...state, isFetching: false, isError: false, resultpositions: payload };
    case POSITION_FAILED:
      return { ...state, isFetching: false, isError: true, resultpositions: null };

      //getUser
    case GETUSER_FETCHING:
      return { ...state, isFetching: true, isError: false, resultgetUser: null };
    case GETUSER_SUCCESS:
      return { ...state, isFetching: false, isError: false, resultgetUser: payload };
    case GETUSER_FAILED:
      return { ...state, isFetching: false, isError: true, resultgetUser: null };

    default:
      return state;
  }
};
