import {
  PERSONAL_FETCHING,
  PERSONAL_SUCCESS,
  PERSONAL_FAILED,
  ADDRESS_FETCHING,
  ADDRESS_SUCCESS,
  ADDRESS_FAILED,
  FAMILY_FETCHING,
  FAMILY_SUCCESS,
  FAMILY_FAILED,
  INITIAL_FETCHING,
  INITIAL_SUCCESS,
  INITIAL_FAILED,
  MILITARY_FETCHING,
  MILITARY_SUCCESS,
  MILITARY_FAILED,
  MARITAL_FETCHING,
  MARITAL_SUCCESS,
  MARITAL_FAILED,
  PROVINCE_FETCHING,
  PROVINCE_SUCCESS,
  PROVINCE_FAILED,
  STATUS_FETCHING,
  STATUS_SUCCESS,
  STATUS_FAILED,
} from "../constants";

const initialState = {
  resultPersonal: null, //personal
  resultAddress: null, //personal
  resultFamily: null, //faily
  resultInitial: null, //initial
  resultMilitary: null, //military
  resultMaterial: null, //material
  resultProvince: null, //province
  resultStatus: null, //status
  isFetching: false,
  isError: false,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    //personal
    case PERSONAL_FETCHING:
      return {
        ...state,
        isFetching: true,
        isError: false,
        resultPersonal: null,
      };
    case PERSONAL_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        resultPersonal: payload,
      };
    case PERSONAL_FAILED:
      return {
        ...state,
        isFetching: false,
        isError: true,
        resultPersonal: null,
      };

    //address
    case ADDRESS_FETCHING:
      return {
        ...state,
        isFetching: true,
        isError: false,
        resultAddress: null,
      };
    case ADDRESS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        resultAddress: payload,
      };
    case ADDRESS_FAILED:
      return {
        ...state,
        isFetching: false,
        isError: true,
        resultAddress: null,
      };

    //family
    case FAMILY_FETCHING:
      return { ...state, isFetching: true, isError: false, resultFamily: null };
    case FAMILY_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        resultFamily: payload,
      };
    case FAMILY_FAILED:
      return { ...state, isFetching: false, isError: true, resultFamily: null };

    //initial
    case INITIAL_FETCHING:
      return {
        ...state,
        isFetching: true,
        isError: false,
        resultInitial: null,
      };
    case INITIAL_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        resultInitial: payload,
      };
    case INITIAL_FAILED:
      return {
        ...state,
        isFetching: false,
        isError: true,
        resultInitial: null,
      };

    //military
    case MILITARY_FETCHING:
      return {
        ...state,
        isFetching: true,
        isError: false,
        resultMilitary: null,
      };
    case MILITARY_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        resultMilitary: payload,
      };
    case MILITARY_FAILED:
      return {
        ...state,
        isFetching: false,
        isError: true,
        resultMilitary: null,
      };

    //material
    case MARITAL_FETCHING:
      return {
        ...state,
        isFetching: true,
        isError: false,
        resultMaterial: null,
      };
    case MARITAL_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        resultMaterial: payload,
      };
    case MARITAL_FAILED:
      return {
        ...state,
        isFetching: false,
        isError: true,
        resultMaterial: null,
      };

    //province
    case PROVINCE_FETCHING:
      return {
        ...state,
        isFetching: true,
        isError: false,
        resultProvince: null,
      };
    case PROVINCE_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        resultProvince: payload,
      };
    case PROVINCE_FAILED:
      return {
        ...state,
        isFetching: false,
        isError: true,
        resultProvince: null,
      };

    //status
    case STATUS_FETCHING:
      return {
        ...state,
        isFetching: true,
        isError: false,
        resultStatus: null,
      };
    case STATUS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        resultStatus: payload,
      };
    case STATUS_FAILED:
      return {
        ...state,
        isFetching: false,
        isError: true,
        resultStatus: null,
      };

    default:
      return state;
  }
};
