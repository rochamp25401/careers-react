import { combineReducers } from "redux";
import registerReducer from "./register.reducer";
import loginReducer from "./login.reducer";
import candidateReducer from "./candidate.reducer";
import personalReducer from "./personal.reducer";

export default combineReducers({
  registerReducer,
  loginReducer,
  candidateReducer,
  personalReducer,
});
