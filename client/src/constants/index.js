export const LOGIN_FETCHING = "LOGIN_FETCHING";
export const LOGIN_FAILED = "LOGIN_FAILED";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGOUT = "LOGOUT";

// Register Page
export const REGISTER_FETCHING = "REGISTER_FETCHING";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAILED = "REGISTER_FAILED";

// candidate
export const CANDIDATE_FETCHING = "CANDIDATE_FETCHING";
export const CANDIDATE_SUCCESS = "CANDIDATE_SUCCESS";
export const CANDIDATE_FAILED = "CANDIDATE_FAILED";

// positions
export const POSITION_FETCHING = "POSITION_FETCHING";
export const POSITION_SUCCESS = "POSITION_SUCCESS";
export const POSITION_FAILED = "POSITION_FAILED";

//personal
export const PERSONAL_FETCHING = "PERSONAL_FETCHING";
export const PERSONAL_SUCCESS = "PERSONAL_SUCCESS";
export const PERSONAL_FAILED = "PERSONAL_FAILED";

//address
export const ADDRESS_FETCHING = "ADDRESS_FETCHING";
export const ADDRESS_SUCCESS = "ADDRESS_SUCCESS";
export const ADDRESS_FAILED = "ADDRESS_FAILED";

//family
export const FAMILY_FETCHING = "FAMILY_FETCHING";
export const FAMILY_SUCCESS = "FAMILY_SUCCESS";
export const FAMILY_FAILED = "FAMILY_FAILED";

//initial
export const INITIAL_FETCHING = "INITIAL_FETCHING";
export const INITIAL_SUCCESS = "INITIAL_SUCCESS";
export const INITIAL_FAILED = "INITIAL_FAILED";

//military
export const MILITARY_FETCHING = "MILITARY_FETCHING";
export const MILITARY_SUCCESS = "MILITARY_SUCCESS";
export const MILITARY_FAILED = "MILITARY_FAILED";

//marital
export const MARITAL_FETCHING = "MARITAL_FETCHING";
export const MARITAL_SUCCESS = "MARITAL_SUCCESS";
export const MARITAL_FAILED = "MARITAL_FAILED";

// provinces
export const PROVINCE_FETCHING = "PROVINCE_FETCHING";
export const PROVINCE_SUCCESS = "PROVINCE_SUCCESS";
export const PROVINCE_FAILED = "PROVINCE_FAILED";

// provinces
export const STATUS_FETCHING = "STATUS_FETCHING";
export const STATUS_SUCCESS = "STATUS_SUCCESS";
export const STATUS_FAILED = "STATUS_FAILED";

// getUser
export const GETUSER_FETCHING = "GETUSER_FETCHING";
export const GETUSER_SUCCESS = "GETUSER_SUCCESS";
export const GETUSER_FAILED = "GETUSER_FAILED";

// // getPersonal
// export const GETPERSONAL_FETCHING = "GETPERSONAL_FETCHING";
// export const GETPERSONAL_SUCCESS = "GETPERSONAL_SUCCESS";
// export const GETPERSONAL_FAILED = "GETPERSONAL_FAILED";

// // getAddress
// export const GETADDRESS_FETCHING = "GETADDRESS_FETCHING";
// export const GETADDRESS_SUCCESS = "GETADDRESS_SUCCESS";
// export const GETADDRESS_FAILED = "GETADDRESS_FAILED";

// // getFamily
// export const GETFAMILY_FETCHING = "GETFAMILY_FETCHING";
// export const GETFAMILY_SUCCESS = "GETFAMILY_SUCCESS";
// export const GETFAMILY_FAILED = "GETFAMILY_FAILED";

// Error Code
export const E_PICKER_CANCELLED = "E_PICKER_CANCELLED";
export const E_PICKER_CANNOT_RUN_CAMERA_ON_SIMULATOR =
  "E_PICKER_CANNOT_RUN_CAMERA_ON_SIMULATOR";
export const E_PERMISSION_MISSING = "E_PERMISSION_MISSING";
export const E_PICKER_NO_CAMERA_PERMISSION = "E_PICKER_NO_CAMERA_PERMISSION";
export const E_USER_CANCELLED = "E_USER_CANCELLED";
export const E_UNKNOWN = "E_UNKNOWN";
export const E_DEVELOPER_ERROR = "E_DEVELOPER_ERROR";
export const TIMEOUT_NETWORK = "ECONNABORTED"; // request service timeout
export const NOT_CONNECT_NETWORK = "NOT_CONNECT_NETWORK";

//////////////// Localization Begin ////////////////
export const NETWORK_CONNECTION_MESSAGE =
  "Cannot connect to server, Please try again.";
export const NETWORK_TIMEOUT_MESSAGE =
  "A network timeout has occurred, Please try again.";
export const UPLOAD_PHOTO_FAIL_MESSAGE =
  "An error has occurred. The photo was unable to upload.";

export const apiUrl = "http://localhost:8080/api/";
export const imageUrl = "http://localhost:8080";

export const server = {
  CANDIDATE_URL: `candidate`,
  LOGIN_URL: `login`,
  REFRESH_TOKEN_URL: `refresh/token`,
  REGISTER_URL: `register`,
  POSITION_URL: `position`,
  PERSONAL_URL: `personal`,
  ADDRESS_URL: `address`,
  FAMILY_URL: `family`,
  INITIAL_URL: `initial`,
  STATUS_URL: `status`,
  MILITARY_URL: `military`,
  MARTIAL_URL: `marital`,
  PROVINCE_URL: `province`,
  GETPERSONAL_URL: `getPersonal`,
  GETADDRESS_URL: `getAddress`,
  GETFAMILY_URL: `getFamily`,
  TOKEN_KEY: `token`,
  REFRESH_TOKEN_KEY: `refresh_token`,
  GETUSER_URL:`getUser`,
};
