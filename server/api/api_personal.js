const express = require("express");
const router = express();
const jwt = require("./../jwt");
const Users = require("./../models/users_schema");
const Personals = require("./../models/personal_schema");
const Initials = require("./../models/initial_schema");
const Militarys = require("./../models/military_schema");
const Marital = require("./../models/marital_schema");
const Provinces = require("./../models/province_schema");
const Address = require("./../models/address_schema");
const Family = require("./../models/family_schema");
const Status = require("./../models/status_schema");

const formidable = require("formidable");
const path = require("path");
const fs = require("fs-extra");

router.get("/getUserEmail", jwt.verify, async (req, res) => {
  console.log(req.jwt);
});

router.put("/personal", jwt.verify, async (req, res) => {
  try {
    console.log(req.body);
    let doc = await Users.findOne({ email: req.jwt.userEmail });
    let doc2 = await Personals.findOneAndUpdate(
      {
        candidateId: doc.candidateId,
      },
      req.body
    );
    if (doc2) {
      res.json({ result: "ok" });
    } else {
      res.json({ result: "nok" });
    }
  } catch (e) {
    res.json({ result: "nokk", message: "internal error" });
    console.log(e);
  }
});

router.put("/address", jwt.verify, async (req, res) => {
  try {
    console.log(req.body);
    let doc = await Users.findOne({ email: req.jwt.userEmail });
    let doc2 = await Address.findOneAndUpdate(
      {
        candidateId: doc.candidateId,
      },
      req.body
    );
    if (doc2) {
      res.json({ result: "ok" });
    } else {
      res.json({ result: "nok" });
    }
  } catch (e) {
    res.json({ result: "nokk", message: "internal error" });
    console.log(e);
  }
});

router.put("/family", jwt.verify, async (req, res) => {
  try {
    console.log(req.body);
    let doc = await Users.findOne({ email: req.jwt.userEmail });
    let doc2 = await Family.findOneAndUpdate(
      {
        candidateId: doc.candidateId,
      },
      req.body
    );
    if (doc2) {
      res.json({ result: "ok" });
    } else {
      res.json({ result: "nok" });
    }
  } catch (e) {
    res.json({ result: "nokk", message: "internal error" });
    console.log(e);
  }
});

router.get("/getPersonal", jwt.verify, async (req, res) => {
  try {
    let doc = await Users.findOne({ email: req.jwt.userEmail });
    let doc2 = await Personals.findOne({
      candidateId: doc.candidateId,
    });
    if(doc2){
      res.json(doc2);
    }
  } catch (e) {
    res.json(e);
  }
});

router.get("/getAddress", jwt.verify, async (req, res) => {
  try {
    let doc = await Users.findOne({ email: req.jwt.userEmail });
    let doc2 = await Address.findOne({
      candidateId: doc.candidateId,
    });
    if(doc2){
      res.json(doc2);
    }
  } catch (e) {
    res.json(e);
  }
});

router.get("/getFamily", jwt.verify, async (req, res) => {
  try {
    let doc = await Users.findOne({ email: req.jwt.userEmail });
    let doc2 = await Family.findOne({
      candidateId: doc.candidateId,
    });
    if(doc2){
      res.json(doc2);
    }
  } catch (e) {
    res.json(e);
  }
});

router.get("/initial", async (req, res) => {
  try {
    const initial = await Initials.find({});
    res.json(initial);
  } catch (e) {
    res.json(e);
  }
});
router.get("/military", async (req, res) => {
  try {
    const military = await Militarys.find({});
    res.json(military);
  } catch (e) {
    res.json(e);
  }
});
router.get("/marital", async (req, res) => {
  try {
    const marital = await Marital.find({});
    res.json(marital);
  } catch (e) {
    res.json(e);
  }
});
router.get("/province", async (req, res) => {
  try {
    const province = await Provinces.find({});
    res.json(province);
  } catch (e) {
    res.json(e);
  }
});

router.get("/status", async (req, res) => {
  try {
    const status = await Status.find({});
    res.json(status);
  } catch (e) {
    res.json(e);
  }
});

router.post("/status", async (req, res) => {
  try {
    const status = await Status.create(req.body);
    res.json(status);
  } catch (e) {
    res.json(e);
  }
});

module.exports = router;
