const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("./../jwt");
const randtoken = require("rand-token");
const refreshTokens = {};
const Users = require("./../models/users_schema");
const Candidates = require("./../models/candidates_schema");
const Personals = require("./../models/personal_schema");
const Address = require("./../models/address_schema");
const Family = require("./../models/family_schema");

router.get("/login", (req, res) => {
  res.end("login");
});

router.post("/login", async (req, res) => {
  try {
    const doc = await Users.findOne({ email: req.body.email });
    if (doc) {
      const isValidPassword = await bcrypt.compare(
        req.body.password,
        doc.password
      );
      if (isValidPassword) {
        const payload = {
          firstname: doc.firstname,
          lastname: doc.lastname,
          email: doc.email,
        };
        const token = jwt.sign(payload, "10000000000000000");
        const refreshToken = randtoken.uid(256);
        refreshTokens[refreshToken] = payload.username;
        res.json({ result: "ok", token, refreshToken, message: "success" });
      } else {
        res.json({ result: "nok", message: "Password is wrong" });
      }
    } else {
      res.json({ result: "nokk", message: "username not found" });
    }
  } catch (e) {
    res.json(e);
  }
});

router.post("/register", async (req, res) => {
  try {
    req.body.password = await bcrypt.hash(req.body.password, 8);
    const doc = await Users.create(req.body);
    const doc2 = await Candidates.create({
      candidateId: doc.candidateId,
      email: req.body.email,
    });
    const doc3 = await Personals.create({
      candidateId: doc.candidateId
    });
    const doc4 = await Address.create({
      candidateId: doc.candidateId
    });
    const doc5 = await Family.create({
      candidateId: doc.candidateId
    });
    if (doc5) {
      res.json({ result: "ok" });
    } else {
      res.json({ result: "nok" });
    }
  } catch (e) {
    res.json({ result: "nokk", message: "internal error" });
    console.log(e); //อยากรู้เฉยๆ
  }
});

// // Refresh Token
// let count = 1;
// router.post("/refresh/token", function (req, res) {
//   const refreshToken = req.body.refreshToken;
//   console.log("Refresh Token : " + count++);
//   console.log(refreshToken)
//   console.log(refreshTokens)
//   if (refreshToken in refreshTokens) {
//     const payload = {
//       username: refreshTokens[refreshToken],
//       level: "normal",
//     };
//     const token = jwt.sign(payload, "100000"); // unit is millisec
//     res.json({ jwt: token });
//   } else {
//     console.log("Not found");
//     return res
//       .status(403)
//       .json({ auth: false, message: "Invalid refresh token" });
//   }
// });

module.exports = router;
