const express = require("express");
const router = express();
const jwt = require("./../jwt");
const Positions = require("./../models/positions_schema");
const Candidates = require("./../models/candidates_schema");
const Users = require("./../models/users_schema");

const formidable = require("formidable");
const path = require("path");
const fs = require("fs-extra");

router.get("/getUser", jwt.verify, async (req, res) => {
  res.json(req.jwt);
});

router.put("/candidate", jwt.verify, async (req, res) => {
  try {
    console.log(req.body);
    let doc = await Users.findOne({ email: req.jwt.userEmail });
    let doc2 = await Candidates.findOneAndUpdate(
      {
        candidateId: doc.candidateId,
      },
      req.body
    );
    if (doc2) {
      res.json({ result: "ok" });
    } else {
      res.json({ result: "nok" });
    }
  } catch (e) {
    res.json({ result: "nokk", message: "internal error" });
    console.log(e);
  }
});

router.get("/candidate", jwt.verify, async (req, res) => {
  try {
    let doc = await Users.findOne({ email: req.jwt.userEmail });
    let doc2 = await Candidates.findOne({
      candidateId: doc.candidateId,
    });
    if(doc2){
      res.json(doc2);
    }
  } catch (e) {
    res.json(e);
  }
});

router.get("/position", async (req, res) => {
  try {
    const position = await Positions.find({});
    res.json(position);
  } catch (e) {
    res.json(e);
  }
});

module.exports = router;
