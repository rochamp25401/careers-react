const express = require("express");
const router = express.Router();

require("./../database/db");
router.use(require("./api_auth"));
router.use(require("./api_candidates"));
router.use(require("./api_personal"));

module.exports = router;
