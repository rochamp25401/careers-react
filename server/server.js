const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
var morgan = require("morgan");

app.use(morgan("tiny"));
app.use(cors());
// app.use(express.static(__dirname + "/uploaded"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use("/api", require("./api/api"));

app.listen(8080, () => {
  console.log("Server is running...");
});
