const mongoose = require("mongoose");
const schema = mongoose.Schema({
  candidateId: Number,
  nameSurname: String,
  age: Number,
  occupation: String,
  phoneNo: Number,
  no: Number,
});

schema.plugin(uniqueValidator);
module.exports = mongoose.model("relatives", schema);
