const mongoose = require("mongoose");
const provincesSchema = mongoose.Schema({
  initialId: Number,
  initialName: String,
});
module.exports = mongoose.model("initials", provincesSchema);
