const mongoose = require("mongoose");
const provincesSchema = mongoose.Schema({
  provinceID: Number,
  provinceEng: String,
});
module.exports = mongoose.model("provinces", provincesSchema);
