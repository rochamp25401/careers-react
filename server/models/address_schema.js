const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const schema = mongoose.Schema({
  candidateId: Number,
  houseNo: String,
  villageNo: String,
  building: String,
  roomNo: String,
  floor: String,
  village: String,
  lane: String,
  road: String,
  subArea: String,
  area: String,
  province: Number,
  zipCode: String,
  phoneNo: Number,
  ext: Number,
});

schema.plugin(uniqueValidator);
module.exports = mongoose.model("address", schema);
