const mongoose = require("mongoose");

const schema = mongoose.Schema({
  statusId: Number,
  statusName: String,
});

module.exports = mongoose.model("status", schema);
