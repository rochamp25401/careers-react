const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const AutoIncrement = require("mongoose-sequence")(mongoose);

const schema = mongoose.Schema({
  firstname: { type: String, required: true, unique: false },
  lastname: String,
  email: { type: String, required: true, unique: true },
  password: String,
});

// schema.index({ email: 1 }, { unique: true });
schema.plugin(AutoIncrement, { inc_field: "candidateId" });
schema.plugin(uniqueValidator);
module.exports = mongoose.model("users", schema);
