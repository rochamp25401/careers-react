const mongoose = require("mongoose");
const schema = mongoose.Schema({
  candidateId: Number,
  name: String,
  surname: String,
  age: Number,
  occupation: String,
  company: String,
  phoneNo: Number,
});
schema.plugin(uniqueValidator);
module.exports = mongoose.model("spouses", schema);
