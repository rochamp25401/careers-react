const mongoose = require("mongoose");

const schema = mongoose.Schema({
  positionId: Number,
  positionName: String,
});

module.exports = mongoose.model("positions", schema);
