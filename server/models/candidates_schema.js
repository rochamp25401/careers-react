const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const AutoIncrement = require("mongoose-sequence")(mongoose);
const schema = mongoose.Schema({
  candidateId: Number,
  position: String,
  email: String,
  availableDate: Date,
  phoneNo: String,
  expectedSalary: Number,
});

schema.plugin(uniqueValidator);
module.exports = mongoose.model("candidates", schema);
